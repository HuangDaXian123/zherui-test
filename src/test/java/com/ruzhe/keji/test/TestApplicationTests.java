package com.ruzhe.keji.test;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.ruzhe.keji.test.dao.CsvInfoDao;
import com.ruzhe.keji.test.dao.VideoTfDao;
import com.ruzhe.keji.test.entity.CsvInfo;
import com.ruzhe.keji.test.entity.Video;
import com.ruzhe.keji.test.entity.VideoInfo;
import com.ruzhe.keji.test.entity.VideoTf;
import com.ruzhe.keji.test.service.VideoInfoService;
import com.ruzhe.keji.test.task.ReadSftpCsv;
import com.ruzhe.keji.test.util.CsvUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.io.*;
import java.util.List;

@SpringBootTest
class TestApplicationTests {
    @Resource
    private VideoTfDao videoTfDao;
    @Autowired(required = false)
    private VideoInfoService videoInfoService;
    @Resource
    private ReadSftpCsv readSftpCsv ;
    @Resource
    private CsvInfoDao csvInfoDao;
    @Test
    void contextLoads() {
        CsvUtil csvUtil=new CsvUtil();
        //File file=new File("G:\\recorder\\camera\\processed\\04-04-0.csv");
        // 将csv文件内容转成bean
        List<VideoTf> list=  csvUtil.getCsvData2(new File("G:\\recorder\\camera\\processed\\04-04-0.csv"), VideoTf.class);
        System.out.println(list.size());
        VideoTf temp =new VideoTf();
        //temp.setVideoId(1);
        for(int  i=1;i<list.size();i++){
            temp=list.get(i);
            System.out.println(temp.toString());
            System.out.println(temp.getEyeLmkx0()+" "+temp.getEyeLmky6());
            //System.out.println(temp.getConfidence());
            //            temp=list.get(i);
//            temp.setId(i);
//            temp.setUserId(1);
//            temp.setCreateTime("04-04");
//            temp.setVideoId(1);
//            videoInfoService.insert(temp);
        }
        //  videoInfoService.deleteByEntity(temp);
        //System.out.println(videoInfoService.countAll());
    }
    @Test
    void test2() throws JSchException, SftpException, IOException {
        readSftpCsv.downLoadCsvAndInsertDB();
    }
    @Test
    void  test3(){
        VideoInfo videoInfo=new VideoInfo();
        CsvInfo csvInfo=new CsvInfo();
        // csvInfo.setCsvname("04-04-0.csv");
        videoInfo.setVideoId(2);
        //csvInfoDao.deleteByEntity(csvInfo);
        videoInfoService.deleteByEntity(videoInfo);

    }
    @Test
    public void test4() throws IOException {
        VideoTf videoTf=new VideoTf();
        File file=new File("E:\\zherui-test\\src\\main\\resources\\writer.txt");
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file,true)));
        for(int i=0;i<8;i++){
            videoTf.setVideoId(i+1);
            videoTf.setCreateTime("2021-04-23-13");
            videoTf.setUserId(1);
            List<VideoTf> list= videoTfDao.listByEntity(videoTf);
            for(int j=0;j<list.size();j++){
                VideoTf videoTf1=new VideoTf();
                videoTf1=list.get(j);
                System.out.println(videoTf1.toString());
                writer.write(String.valueOf(Double.valueOf(videoTf1.getEyeLmkx8())-Double.valueOf(videoTf1.getEyeLmkx14()))+","+String.valueOf(Double.valueOf(videoTf1.getEyeLmkx11())-Double.valueOf(videoTf1.getEyeLmkx17()))+","+String.valueOf(Double.valueOf(videoTf1.getEyeLmkx0())-Double.valueOf(videoTf1.getEyeLmkx4()))+","+String.valueOf(Double.valueOf(videoTf1.getEyeLmkx2())-Double.valueOf(videoTf1.getEyeLmkx6()))+","
                        +String.valueOf(Double.valueOf(videoTf1.getEyeLmkx36())-Double.valueOf(videoTf1.getEyeLmkx42()))+","+String.valueOf(Double.valueOf(videoTf1.getEyeLmkx39())-Double.valueOf(videoTf1.getEyeLmkx45()))+","+String.valueOf(Double.valueOf(videoTf1.getEyeLmkx28())-Double.valueOf(videoTf1.getEyeLmkx32()))+","+String.valueOf(Double.valueOf(videoTf1.getEyeLmkx30())-Double.valueOf(videoTf1.getEyeLmkx34())));
                writer.newLine();
                writer.write(String.valueOf(Double.valueOf(videoTf1.getEyeLmky8())-Double.valueOf(videoTf1.getEyeLmky14()))+","+String.valueOf(Double.valueOf(videoTf1.getEyeLmky11())-Double.valueOf(videoTf1.getEyeLmky17()))+","+String.valueOf(Double.valueOf(videoTf1.getEyeLmky0())-Double.valueOf(videoTf1.getEyeLmky4()))+","+String.valueOf(Double.valueOf(videoTf1.getEyeLmky2())-Double.valueOf(videoTf1.getEyeLmky6()))+","
                        +String.valueOf(Double.valueOf(videoTf1.getEyeLmky36())-Double.valueOf(videoTf1.getEyeLmky42()))+","+String.valueOf(Double.valueOf(videoTf1.getEyeLmky39())-Double.valueOf(videoTf1.getEyeLmky45()))+","+String.valueOf(Double.valueOf(videoTf1.getEyeLmky28())-Double.valueOf(videoTf1.getEyeLmky32()))+","+String.valueOf(Double.valueOf(videoTf1.getEyeLmky30())-Double.valueOf(videoTf1.getEyeLmky34())));
                writer.newLine();
            }
        }
        writer.close();

    }
    @Test
    public  void testFour()
    {
        VideoTf videoTf=new VideoTf();
        videoTf.setVideoId(1);
        videoTf.setCreateTime("2021-04-23-13");
        videoTf.setUserId(1);
        List<VideoTf> list= videoTfDao.listByEntity(videoTf);
        for(int j=0;j<list.size();j++){
           VideoTf videoTf1=new VideoTf();
           videoTf1=list.get(j);
            System.out.println(videoTf1.toString());
            System.out.println(videoTf1.getFrame());
        }
    }


}
