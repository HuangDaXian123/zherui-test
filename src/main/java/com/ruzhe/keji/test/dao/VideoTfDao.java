package com.ruzhe.keji.test.dao;

import com.ruzhe.keji.test.entity.VideoTf;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface VideoTfDao {

    VideoTf getById(Integer id);

    List<VideoTf> listByEntity(VideoTf videoTf);

    VideoTf getByEntity(VideoTf videoTf);

    List<VideoTf> listByIds(List<Integer> list);

    int insert(VideoTf videoTf);

    int insertBatch(List<VideoTf> list);

    int update(VideoTf videoTf);

    int updateByField(@Param("where") VideoTf where, @Param("set") VideoTf set);

    int updateBatch(List<VideoTf> list);

    int deleteById(Integer id);

    int deleteByEntity(VideoTf videoTf);

    int deleteByIds(List<Integer> list);

    int countAll();

    int countByEntity(VideoTf videoTf);

}