package com.ruzhe.keji.test.dao;

import com.ruzhe.keji.test.entity.Video;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface VideoDao {

    Video getById(Integer id);

    List<Video> listByEntity(Video video);

    Video getByEntity(Video video);

    List<Video> listByIds(List<Integer> list);

    int insert(Video video);

    int insertBatch(List<Video> list);

    int update(Video video);

    int updateByField(@Param("where") Video where, @Param("set") Video set);

    int updateBatch(List<Video> list);

    int deleteById(Integer id);

    int deleteByEntity(Video video);

    int deleteByIds(List<Integer> list);

    int countAll();

    int countByEntity(Video video);

}