package com.ruzhe.keji.test.dao;

import com.ruzhe.keji.test.entity.CsvInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CsvInfoDao {

    CsvInfo getById(Integer id);

    List<CsvInfo> listByEntity(CsvInfo csvInfo);

    CsvInfo getByEntity(CsvInfo csvInfo);

    List<CsvInfo> listByIds(List<Integer> list);

    @Options(useGeneratedKeys = true,keyProperty = "id",keyColumn = "id")
    int insert(CsvInfo csvInfo);

    int insertBatch(List<CsvInfo> list);

    int update(CsvInfo csvInfo);

    int updateByField(@Param("where") CsvInfo where, @Param("set") CsvInfo set);

    int updateBatch(List<CsvInfo> list);

    int deleteById(Integer id);

    int deleteByEntity(CsvInfo csvInfo);

    int deleteByIds(List<Integer> list);

    int countAll();

    int countByEntity(CsvInfo csvInfo);

}