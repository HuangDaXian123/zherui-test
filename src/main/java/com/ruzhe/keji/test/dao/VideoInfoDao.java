package com.ruzhe.keji.test.dao;

import com.ruzhe.keji.test.entity.VideoInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface VideoInfoDao {

    VideoInfo getById(Integer id);

    List<VideoInfo> listByEntity(VideoInfo videoInfo);

    VideoInfo getByEntity(VideoInfo videoInfo);

    List<VideoInfo> listByIds(List<Integer> list);

    @Options(useGeneratedKeys = true,keyProperty = "id",keyColumn = "id")
    int insert(VideoInfo videoInfo);

    int insertBatch(List<VideoInfo> list);

    int update(VideoInfo videoInfo);

    int updateByField(@Param("where") VideoInfo where, @Param("set") VideoInfo set);

    int updateBatch(List<VideoInfo> list);

    int deleteById(Integer id);

    int deleteByEntity(VideoInfo videoInfo);

    int deleteByIds(List<Integer> list);

    int countAll();

    int countByEntity(VideoInfo videoInfo);

}