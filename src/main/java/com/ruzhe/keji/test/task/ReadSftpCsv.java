package com.ruzhe.keji.test.task;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import  com.ruzhe.keji.test.dao.CsvInfoDao;
import  com.ruzhe.keji.test.dao.VideoInfoDao;
import com.ruzhe.keji.test.dao.VideoTfDao;
import  com.ruzhe.keji.test.entity.CsvInfo;
import  com.ruzhe.keji.test.entity.SftpConfig;
import  com.ruzhe.keji.test.entity.VideoInfo;
import com.ruzhe.keji.test.entity.VideoTf;
import  com.ruzhe.keji.test.util.CsvUtil;
import  com.ruzhe.keji.test.util.DataUtils;
import com.ruzhe.keji.test.util.SftpUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;


//连接Stfp读取CSV文件
@Service
public class ReadSftpCsv {
    @Autowired(required = false)
    private SftpConfig sftpConfig;

    @Resource
    private VideoInfoDao videoInfoDao;

    @Resource
    private CsvInfoDao csvInfoDao;

    @Resource
    private VideoTfDao videoTfDao;

    @Resource
    private  CsvUtil csvUtil;


    public static final String FILE_SEPARATOR = File.separator;

    public void downLoadCsvAndInsertDB() throws SftpException, JSchException, IOException {
        //文件前缀(要保存到本地的文件),并保存到数据库
        String prefix = DataUtils.getCurrentDate("yyyyMMddHHmmss");
        //文件名
       // String saveFileName = prefix + ".csv";
        //文件存放路径
        String path = sftpConfig.getSavePath();
        System.out.println("downloadCsv path: "+path);
        //从sftp下载文件
        boolean flag = this.downLoadFile(path);
        //下载成功后读取文件，失败不读取
        if(flag){
            //读取.csv文件,批量插入数据
            this.ReadCsv();
            System.out.println(prefix+"数据库更新成功！");
        }else{
            System.out.println("数据库更新失败！");
        }

    }

    /*
        获取最新的csv文件
     */
    public  List<String> getNewestFilesName() throws SftpException, JSchException {
        System.out.println(sftpConfig.toString());
        //连接stfp
        ChannelSftp sftp = SftpUtils.login(sftpConfig.getUsername(), sftpConfig.getPassword(), sftpConfig.getHost(), sftpConfig.getPort());
        sftp.cd(sftpConfig.getDir());
        List<String> list = new ArrayList<>();
        CsvInfo csvInfo=new CsvInfo();
        //获取sftp目录下所有文件
        Vector v = sftp.ls(sftpConfig.getDir());
        if (v.size() > 0) {
            Iterator it = v.iterator();
            while (it.hasNext()) {  //遍历某路径下的所有文件
                ChannelSftp.LsEntry entry = (ChannelSftp.LsEntry) it.next();
                String fileNameCsv = entry.getFilename();//路径下某个文件名称名称
                if(fileNameCsv.contains("csv")){
                    System.out.println(fileNameCsv);
                    csvInfo.setCsvname(fileNameCsv);
                   // CsvInfo info=csvInfoDao.getByEntity(csvInfo);
                  //  System.out.println(info.toString());
                    if(csvInfoDao.getByEntity(csvInfo)==null){
                        list.add(fileNameCsv);
                    }

                }
            }
        }
        return  list;
    }

    /**
     * 下载文件
     * @param path 路径+文件名 本地保存文件路径
     */
    public boolean downLoadFile(String path){

        //执行读取文件的标识，
        boolean flag = true;
        //String downLoadFileName = sftpConfig.getFileNamePrefix() + DataUtils.getPreDate("yyyyMM") + ".csv";
        //sftp 文件名
        String downLoadFileName = "";
        try {
            //连接stfp
            ChannelSftp sftp = SftpUtils.login(sftpConfig.getUsername(), sftpConfig.getPassword(), sftpConfig.getHost(), sftpConfig.getPort());
            sftp.cd(sftpConfig.getDir());
            //创建文件


            //创建新文件


            List<String>list=getNewestFilesName();
            //下载更新的文件
            for(int i=0;i<list.size();i++){
                downLoadFileName=list.get(i);
                File file = new File(ResourceUtils.getFile("classpath:download").getAbsolutePath()+"//"+downLoadFileName);
                System.out.println(file.getAbsolutePath());
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                sftp.get(downLoadFileName, fileOutputStream);
                //关闭流
                fileOutputStream.close();
            }

            //退出sftp
            SftpUtils.logout();
        } catch (Exception e) {
            flag = false;
            e.printStackTrace();
        }
        return flag;
    }

    /**
     * 读取.csv文件并将csv插入数据库
     *
     * @return
     */
    public void ReadCsv() throws JSchException, SftpException, IOException {
        // 将csv文件内容转成bean

        List<String> filenames=getNewestFilesName();
        List<CsvInfo>csvInfos=new ArrayList<>();

        for(int i=0;i<filenames.size();i++){//遍历没有记录过的文件
            int userId;
            String createTime;
            int videoId;

            CsvInfo csvInfo=new CsvInfo();
            String filename=filenames.get(i);//获取文件名
            csvInfo.setCsvname(filename);//将未记录的文件名添加到队列中

            //对文件名进行处理获取userId,video_id,create_time
            String []arg=filename.split(".csv");
            String []arg1=arg[0].split("_");
            userId=Integer.parseInt(arg1[0]);
            createTime=arg1[1];
            videoId=Integer.parseInt(arg1[2])+1;
           // csvInfos.add(csvInfo);

            //将对应的视频信息转为javabean
            //VideoInfo 类型的java bean
            File file= ResourceUtils.getFile("classpath:download"+"/"+filename);
            List<VideoInfo> list=  csvUtil.getCsvData(file,VideoInfo.class);
            List<VideoTf> tfList=csvUtil.getCsvData2(file,VideoTf.class);
            System.out.println(list.size()+" "+tfList.size());
            VideoInfo temp =new VideoInfo();
            VideoTf videoTf=new VideoTf();
            //更新文件名
            csvInfoDao.insert(csvInfo);
            //对数据库进行更新操作
            for(int  j=1;j<tfList.size();j++){
                videoTf=tfList.get(j);
                // temp.setId(j+times);
                videoTf.setUserId(userId);//设置userId
                videoTf.setCreateTime(createTime);//设置createTime
                videoTf.setVideoId(videoId);//设置videoId
                // videoInfoList.add(temp);
                //将数据插入
                videoTfDao.insert(videoTf);
            }

            for(int  j=0;j<list.size();j++){
                temp=list.get(j);
               // temp.setId(j+times);
                temp.setUserId(userId);//设置userId
                temp.setCreateTime(createTime);//设置createTime
                temp.setVideoId(videoId);//设置videoId
               // videoInfoList.add(temp);
                //将数据插入
                videoInfoDao.insert(temp);
            }


        }

    }

}
