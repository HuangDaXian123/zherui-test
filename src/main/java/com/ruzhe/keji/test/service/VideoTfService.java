package com.ruzhe.keji.test.service;

import com.ruzhe.keji.test.dao.VideoTfDao;
import com.ruzhe.keji.test.entity.VideoTf;

import java.util.List;

public interface VideoTfService {

    VideoTfDao getVideoTfDao();

    VideoTf getById(Integer id);

    VideoTf getByEntity(VideoTf videoTf);

    List<VideoTf> listByEntity(VideoTf videoTf);

    List<VideoTf> listByIds(List<Integer> ids);

    int insert(VideoTf videoTf);

    int insertBatch(List<VideoTf> list);

    int update(VideoTf videoTf);

    int updateBatch(List<VideoTf> list);

    int deleteById(Integer id);

    int deleteByEntity(VideoTf videoTf);

    int deleteByIds(List<Integer> list);

    int countAll();

    int countByEntity(VideoTf videoTf);
}