package com.ruzhe.keji.test.service;

import com.ruzhe.keji.test.dao.CsvInfoDao;
import com.ruzhe.keji.test.entity.CsvInfo;

import java.util.List;

public interface CsvInfoService {

    CsvInfoDao getCsvInfoDao();

    CsvInfo getById(Integer id);

    CsvInfo getByEntity(CsvInfo csvInfo);

    List<CsvInfo> listByEntity(CsvInfo csvInfo);

    List<CsvInfo> listByIds(List<Integer> ids);

    int insert(CsvInfo csvInfo);

    int insertBatch(List<CsvInfo> list);

    int update(CsvInfo csvInfo);

    int updateBatch(List<CsvInfo> list);

    int deleteById(Integer id);

    int deleteByEntity(CsvInfo csvInfo);

    int deleteByIds(List<Integer> list);

    int countAll();

    int countByEntity(CsvInfo csvInfo);
}