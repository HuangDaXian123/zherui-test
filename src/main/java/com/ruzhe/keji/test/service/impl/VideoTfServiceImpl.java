package com.ruzhe.keji.test.service.impl;

import com.ruzhe.keji.test.dao.VideoTfDao;
import com.ruzhe.keji.test.entity.VideoTf;
import com.ruzhe.keji.test.service.VideoTfService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class VideoTfServiceImpl implements VideoTfService {

    @Resource(type = VideoTfDao.class)
    private VideoTfDao videoTfDao;

    @Override
    public VideoTfDao getVideoTfDao() {
        return videoTfDao;
    }

    public VideoTf getById(Integer id) {
        return videoTfDao.getById(id);
    }

    public VideoTf getByEntity(VideoTf videoTf) {
        return videoTfDao.getByEntity(videoTf);
    }

    public List<VideoTf> listByEntity(VideoTf videoTf) {
        return videoTfDao.listByEntity(videoTf);
    }

    public List<VideoTf> listByIds(List<Integer> ids) {
        return videoTfDao.listByIds(ids);
    }

    public int insert(VideoTf videoTf) {
        return videoTfDao.insert(videoTf);
    }

    public int insertBatch(List<VideoTf> list) {
        return videoTfDao.insertBatch(list);
    }

    public int update(VideoTf videoTf) {
        return videoTfDao.update(videoTf);
    }

    public int updateBatch(List<VideoTf> list) {
        return videoTfDao.updateBatch(list);
    }

    public int deleteById(Integer id) {
        return videoTfDao.deleteById(id);
    }

    public int deleteByEntity(VideoTf videoTf) {
        return videoTfDao.deleteByEntity(videoTf);
    }

    public int deleteByIds(List<Integer> list) {
        return videoTfDao.deleteByIds(list);
    }

    public int countAll() {
        return videoTfDao.countAll();
    }

    public int countByEntity(VideoTf videoTf) {
        return videoTfDao.countByEntity(videoTf);
    }

}