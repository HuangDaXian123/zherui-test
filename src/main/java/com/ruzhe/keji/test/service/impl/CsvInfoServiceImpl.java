package com.ruzhe.keji.test.service.impl;

import  com.ruzhe.keji.test.dao.CsvInfoDao;
import  com.ruzhe.keji.test.entity.CsvInfo;
import  com.ruzhe.keji.test.service.CsvInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CsvInfoServiceImpl implements CsvInfoService {

    @Resource(type = CsvInfoDao.class)
    private CsvInfoDao csvInfoDao;

    @Override
    public CsvInfoDao getCsvInfoDao() {
        return csvInfoDao;
    }

    public CsvInfo getById(Integer id) {
        return csvInfoDao.getById(id);
    }

    public CsvInfo getByEntity(CsvInfo csvInfo) {
        return csvInfoDao.getByEntity(csvInfo);
    }

    public List<CsvInfo> listByEntity(CsvInfo csvInfo) {
        return csvInfoDao.listByEntity(csvInfo);
    }

    public List<CsvInfo> listByIds(List<Integer> ids) {
        return csvInfoDao.listByIds(ids);
    }

    public int insert(CsvInfo csvInfo) {
        return csvInfoDao.insert(csvInfo);
    }

    public int insertBatch(List<CsvInfo> list) {
        return csvInfoDao.insertBatch(list);
    }

    public int update(CsvInfo csvInfo) {
        return csvInfoDao.update(csvInfo);
    }

    public int updateBatch(List<CsvInfo> list) {
        return csvInfoDao.updateBatch(list);
    }

    public int deleteById(Integer id) {
        return csvInfoDao.deleteById(id);
    }

    public int deleteByEntity(CsvInfo csvInfo) {
        return csvInfoDao.deleteByEntity(csvInfo);
    }

    public int deleteByIds(List<Integer> list) {
        return csvInfoDao.deleteByIds(list);
    }

    public int countAll() {
        return csvInfoDao.countAll();
    }

    public int countByEntity(CsvInfo csvInfo) {
        return csvInfoDao.countByEntity(csvInfo);
    }

}