package com.ruzhe.keji.test.service;

import  com.ruzhe.keji.test.dao.VideoDao;
import  com.ruzhe.keji.test.entity.Video;

import java.util.List;

public interface VideoService {

    VideoDao getVideoDao();

    Video getById(Integer id);

    Video getByEntity(Video video);

    List<Video> listByEntity(Video video);

    List<Video> listByIds(List<Integer> ids);

    int insert(Video video);

    int insertBatch(List<Video> list);

    int update(Video video);

    int updateBatch(List<Video> list);

    int deleteById(Integer id);

    int deleteByEntity(Video video);

    int deleteByIds(List<Integer> list);

    int countAll();

    int countByEntity(Video video);
}