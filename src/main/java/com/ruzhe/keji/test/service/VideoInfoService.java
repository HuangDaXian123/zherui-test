package com.ruzhe.keji.test.service;

import  com.ruzhe.keji.test.dao.VideoInfoDao;
import  com.ruzhe.keji.test.entity.VideoInfo;

import java.util.List;

public interface VideoInfoService {

    VideoInfoDao getVideoInfoDao();

    VideoInfo getById(Integer id);

    VideoInfo getByEntity(VideoInfo videoInfo);

    List<VideoInfo> listByEntity(VideoInfo videoInfo);

    List<VideoInfo> listByIds(List<Integer> ids);

    int insert(VideoInfo videoInfo);

    int insertBatch(List<VideoInfo> list);

    int update(VideoInfo videoInfo);

    int updateBatch(List<VideoInfo> list);

    int deleteById(Integer id);

    int deleteByEntity(VideoInfo videoInfo);

    int deleteByIds(List<Integer> list);

    int countAll();

    int countByEntity(VideoInfo videoInfo);
}