package com.ruzhe.keji.test.service.impl;

import  com.ruzhe.keji.test.dao.VideoInfoDao;
import  com.ruzhe.keji.test.entity.VideoInfo;
import  com.ruzhe.keji.test.service.VideoInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class VideoInfoServiceImpl implements VideoInfoService {

    @Resource(type = VideoInfoDao.class)
    private VideoInfoDao videoInfoDao;

    @Override
    public VideoInfoDao getVideoInfoDao() {
        return videoInfoDao;
    }

    public VideoInfo getById(Integer id) {
        return videoInfoDao.getById(id);
    }

    public VideoInfo getByEntity(VideoInfo videoInfo) {
        return videoInfoDao.getByEntity(videoInfo);
    }

    public List<VideoInfo> listByEntity(VideoInfo videoInfo) {
        return videoInfoDao.listByEntity(videoInfo);
    }

    public List<VideoInfo> listByIds(List<Integer> ids) {
        return videoInfoDao.listByIds(ids);
    }

    public int insert(VideoInfo videoInfo) {
        return videoInfoDao.insert(videoInfo);
    }

    public int insertBatch(List<VideoInfo> list) {
        return videoInfoDao.insertBatch(list);
    }

    public int update(VideoInfo videoInfo) {
        return videoInfoDao.update(videoInfo);
    }

    public int updateBatch(List<VideoInfo> list) {
        return videoInfoDao.updateBatch(list);
    }

    public int deleteById(Integer id) {
        return videoInfoDao.deleteById(id);
    }

    public int deleteByEntity(VideoInfo videoInfo) {
        return videoInfoDao.deleteByEntity(videoInfo);
    }

    public int deleteByIds(List<Integer> list) {
        return videoInfoDao.deleteByIds(list);
    }

    public int countAll() {
        return videoInfoDao.countAll();
    }

    public int countByEntity(VideoInfo videoInfo) {
        return videoInfoDao.countByEntity(videoInfo);
    }

}