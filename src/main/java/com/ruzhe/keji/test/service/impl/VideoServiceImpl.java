package com.ruzhe.keji.test.service.impl;

import  com.ruzhe.keji.test.dao.VideoDao;
import  com.ruzhe.keji.test.entity.Video;
import  com.ruzhe.keji.test.service.VideoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class VideoServiceImpl implements VideoService {

    @Resource(type = VideoDao.class)
    private VideoDao videoDao;

    @Override
    public VideoDao getVideoDao() {
        return videoDao;
    }

    public Video getById(Integer id) {
        return videoDao.getById(id);
    }

    public Video getByEntity(Video video) {
        return videoDao.getByEntity(video);
    }

    public List<Video> listByEntity(Video video) {
        return videoDao.listByEntity(video);
    }

    public List<Video> listByIds(List<Integer> ids) {
        return videoDao.listByIds(ids);
    }

    public int insert(Video video) {
        return videoDao.insert(video);
    }

    public int insertBatch(List<Video> list) {
        return videoDao.insertBatch(list);
    }

    public int update(Video video) {
        return videoDao.update(video);
    }

    public int updateBatch(List<Video> list) {
        return videoDao.updateBatch(list);
    }

    public int deleteById(Integer id) {
        return videoDao.deleteById(id);
    }

    public int deleteByEntity(Video video) {
        return videoDao.deleteByEntity(video);
    }

    public int deleteByIds(List<Integer> list) {
        return videoDao.deleteByIds(list);
    }

    public int countAll() {
        return videoDao.countAll();
    }

    public int countByEntity(Video video) {
        return videoDao.countByEntity(video);
    }

}