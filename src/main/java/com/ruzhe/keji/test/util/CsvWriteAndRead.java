package com.ruzhe.keji.test.util;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CsvWriteAndRead{

    public static boolean exportCsv(File file, List<String> dataList){
        boolean isSucess=false;
        FileOutputStream out=null;
        OutputStreamWriter osw=null;
        BufferedWriter bw=null;
        try {
            out = new FileOutputStream(file);
            osw = new OutputStreamWriter(out);
            bw =new BufferedWriter(osw);
            if(dataList!=null && !dataList.isEmpty()){
                for(String data : dataList){
                    bw.append(data).append("\r");
                }
            }
            isSucess=true;
        } catch (Exception e) {
            isSucess=false;
        }finally{
            if(bw!=null){
                try {
                    bw.close();
                    bw=null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(osw!=null){
                try {
                    osw.close();
                    osw=null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(out!=null){
                try {
                    out.close();
                    out=null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return isSucess;
    }
    

    /**
      * 读取
      * @param file csv文件(路径+文件)
      * @return
      */
    public static List<String> importCsv(File file){
        List<String> dataList=new ArrayList<String>();
    
        BufferedReader br=null;
        try { 
            br = new BufferedReader(new FileReader(file));
            String line = ""; 
            while ((line = br.readLine()) != null) { 
                dataList.add(line);
            }
        }catch (Exception e) {
        }finally{
            if(br!=null){
                try {
                    br.close();
                    br=null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
 
        return dataList;
    }

    public static void importCsv() throws IOException {
        List<String> dataList=CsvWriteAndRead.importCsv(new File("G:\\recorder\\camera\\processed\\04-04-0.csv"));
        File file=new File("E:\\zherui-test\\src\\main\\resources\\test2.txt");
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file,true)));
        if(dataList!=null && !dataList.isEmpty()){
            for(int i=0; i<dataList.size();i++ ){
                if(i==0){//读取第一行
                    String s=dataList.get(i);
                    System.out.println(s);
                    String[] as = s.split(",");
                    for(int j=0;j<as.length;j++){
                        System.out.print(as[j]+" ");
                        //writer.write(" @CsvBindByName(column =\""+as[j].trim()+"\")");
                        writer.write("@CsvBindByPosition(position ="+j+","+"capture = TRIM)");
                       // writer.newLine();
                        //writer.write("@Column(length = 20)");
                        String temp=as[j];
                        if(temp.contains("X")){
                           temp= temp.replace("X","Xy");
                        }else if(temp.contains("Y")){
                           temp= temp.replace("Y","Yz");
                        }else if(temp.contains("Z")){
                           temp= temp.replace("Z","Zx");
                        }else {
                            temp=as[j];
                        }
                        temp=HumpLineUtils.transHump(temp);
                        writer.newLine();
                        writer.write("private Double "+temp+";");
                        writer.newLine();
                    }
                }
            }
            System.out.println("写完成！");
            writer.close();
        }

    }
    public static void main(String[] args) throws IOException {
        importCsv();
    }
}