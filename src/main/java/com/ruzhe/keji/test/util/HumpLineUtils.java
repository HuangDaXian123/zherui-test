package com.ruzhe.keji.test.util;

public class HumpLineUtils {
    //转驼峰命名的工具类
    /**
     * 例如：helloWorld->hello_world
     */
    public static String transline(String name,String type) {
        StringBuilder result = new StringBuilder();
        if (name != null && name.length() > 0) {
            // 循环处理其余字符
            for (int i = 0; i < name.length(); i++) {
                if(i==0&&"Lower".equals(type)) {
                    result.append(name.substring(0, 1).toLowerCase());
                    continue;
                }
                if(i==0) {
                    result.append(name.substring(0, 1).toUpperCase());
                    continue;
                }
                String s = name.substring(i, i + 1);
                // 在大写字母前添加下划线
                if (s.equals(s.toUpperCase()) && !Character.isDigit(s.charAt(0))) {
                    result.append("_");
                }
                //其他转化小写
                if("Lower".equals(type)) {
                    result.append(s.toLowerCase());
                    continue;
                }
                // 其他字符直接转成大写
                result.append(s.toUpperCase());
            }
        }
        return result.toString();
    }
    /**
     * hello_world-->helloWorld
     * 例如：HELLO_WORLD->HelloWorld
     */
    public static String transHump(String name) {
        StringBuilder result = new StringBuilder();
        // 快速检查
        if (name == null || name.isEmpty()) {
            // 没必要转换
            return "";
        } else if (!name.contains("_")) {
            // 不含下划线，仅将首字母小写
            return name.substring(0, 1).toLowerCase() + name.substring(1);
        }
        // 用下划线将原始字符串分割
        String camels[] = name.split("_");
        for (String camel :  camels) {
            // 跳过原始字符串中开头、结尾的下换线或双重下划线
            if (camel.isEmpty()) {
                continue;
            }
            // 处理真正的驼峰片段
            if (result.length() == 0) {
                // 第一个驼峰片段，全部字母都小写
                result.append(camel.toLowerCase());
            } else {
                // 其他的驼峰片段，首字母大写
                result.append(camel.substring(0, 1).toUpperCase());
                result.append(camel.substring(1).toLowerCase());
            }
        }
        return result.toString();
    }

}
