package com.ruzhe.keji.test.util;

import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Component
public class DataUtils {
    /**
     * 获取当前时间
     * @param format 格式 例如：yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static String getCurrentDate(String format){
        SimpleDateFormat s = new SimpleDateFormat(format);
        return s.format(new Date());
    }

    /**
     * 获取上一个月
     * @param format 格式 例如：yyyy-MM
     */
    public static String getPreDate(String format){
        SimpleDateFormat ss = new SimpleDateFormat(format);
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.MONTH, -1);
        return ss.format(c.getTime());
    }

    public static void main (String[] args){
        System.out.println(getCurrentDate("yyyy-MM-dd-HH-mm-ss"));
        System.out.println(getPreDate("yyyy-MM"));
    }


}
