package com.ruzhe.keji.test.entity;

import java.io.Serializable;

/**
 * (CsvInfo)实体类
 *
 * @author huangdaxian
 * @since 2021-04-15 22:36:39
 */
public class CsvInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    /**
     * 已记录的csv文件名
     */
    private String csvname;

    public CsvInfo() {
    }

    public CsvInfo(Integer id, String csvname) {
        this.id = id;
        this.csvname = csvname;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCsvname() {
        return csvname;
    }

    public void setCsvname(String csvname) {
        this.csvname = csvname;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("CsvInfo{");
        str.append("id=").append(this.id).append(", ");
        str.append("csvname=").append(this.csvname);
        str.append('}');
        return str.toString();
    }

}