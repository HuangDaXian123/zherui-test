package com.ruzhe.keji.test.entity;

import com.opencsv.bean.CsvBindByName;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity //实体类的注解
@Getter
@Setter
@NoArgsConstructor
public class VideoInfo {

   @Id
   @GeneratedValue
   private Integer id;

   private Integer userId;
   private Integer videoId;
   private String createTime;

   @CsvBindByName(column ="frame",required = true)
   private Double frame;
   @CsvBindByName(column ="face_id")
   private Double  faceId;
   @CsvBindByName(column ="timestamp")
   private Double  timestamp;
   @CsvBindByName(column ="confidence")
   private Double  confidence;
   @CsvBindByName(column ="success")
   private Double  success;
   @CsvBindByName(column ="gaze_0_x")
   private Double  gaze0x;
   @CsvBindByName(column ="gaze_0_y")
   private Double  gaze0y;
   @CsvBindByName(column ="gaze_0_z")
   private Double  gaze0z;
   @CsvBindByName(column ="gaze_1_x")
   private Double  gaze1x;
   @CsvBindByName(column ="gaze_1_y")
   private Double  gaze1y;
   @CsvBindByName(column ="gaze_1_z")
   private Double  gaze1z;
   @CsvBindByName(column ="gaze_angle_x")
   private Double  gazeAnglex;
   @CsvBindByName(column ="gaze_angle_y")
   private Double  gazeAngley;
   @CsvBindByName(column =" eye_lmk_x_0")
   private Double  eyeLmkx0;
   @CsvBindByName(column =" eye_lmk_x_1")
   private Double  eyeLmkx1;
   @CsvBindByName(column =" eye_lmk_x_2")
   private Double  eyeLmkx2;
   @CsvBindByName(column =" eye_lmk_x_3")
   private Double  eyeLmkx3;
   @CsvBindByName(column ="eye_lmk_x_4")
   private Double  eyeLmkx4;
   @CsvBindByName(column =" eye_lmk_x_5")
   private Double  eyeLmkx5;
   @CsvBindByName(column =" eye_lmk_x_6")
   private Double  eyeLmkx6;
   @CsvBindByName(column ="eye_lmk_x_7")
   private Double  eyeLmkx7;
   @CsvBindByName(column =" eye_lmk_x_8")
   private Double  eyeLmkx8;
   @CsvBindByName(column =" eye_lmk_x_9")
   private Double  eyeLmkx9;
   @CsvBindByName(column =" eye_lmk_x_10")
   private Double  eyeLmkx10;
   @CsvBindByName(column ="eye_lmk_x_11")
   private Double  eyeLmkx11;
   @CsvBindByName(column =" eye_lmk_x_12")
   private Double  eyeLmkx12;
   @CsvBindByName(column =" eye_lmk_x_13")
   private Double  eyeLmkx13;
   @CsvBindByName(column =" eye_lmk_x_14")
   private Double  eyeLmkx14;
   @CsvBindByName(column =" eye_lmk_x_15")
   private Double  eyeLmkx15;
   @CsvBindByName(column =" eye_lmk_x_16")
   private Double  eyeLmkx16;
   @CsvBindByName(column =" eye_lmk_x_17")
   private Double  eyeLmkx17;
   @CsvBindByName(column =" eye_lmk_x_18")
   private Double  eyeLmkx18;
   @CsvBindByName(column =" eye_lmk_x_19")
   private Double  eyeLmkx19;
   @CsvBindByName(column =" eye_lmk_x_20")
   private Double  eyeLmkx20;
   @CsvBindByName(column ="eye_lmk_x_21")
   private Double  eyeLmkx21;
   @CsvBindByName(column ="eye_lmk_x_22")
   private Double  eyeLmkx22;
   @CsvBindByName(column =" eye_lmk_x_23")
   private Double  eyeLmkx23;
   @CsvBindByName(column =" eye_lmk_x_24")
   private Double  eyeLmkx24;
   @CsvBindByName(column =" eye_lmk_x_25")
   private Double  eyeLmkx25;
   @CsvBindByName(column ="eye_lmk_x_26")
   private Double  eyeLmkx26;
   @CsvBindByName(column =" eye_lmk_x_27")
   private Double  eyeLmkx27;
   @CsvBindByName(column =" eye_lmk_x_28")
   private Double  eyeLmkx28;
   @CsvBindByName(column ="eye_lmk_x_29")
   private Double  eyeLmkx29;
   @CsvBindByName(column =" eye_lmk_x_30")
   private Double  eyeLmkx30;
   @CsvBindByName(column ="eye_lmk_x_31")
   private Double  eyeLmkx31;
   @CsvBindByName(column ="eye_lmk_x_32")
   private Double  eyeLmkx32;
   @CsvBindByName(column ="eye_lmk_x_33")
   private Double  eyeLmkx33;
   @CsvBindByName(column =" eye_lmk_x_34")
   private Double  eyeLmkx34;
   @CsvBindByName(column ="eye_lmk_x_35")
   private Double  eyeLmkx35;
   @CsvBindByName(column =" eye_lmk_x_36")
   private Double  eyeLmkx36;
   @CsvBindByName(column =" eye_lmk_x_37")
   private Double  eyeLmkx37;
   @CsvBindByName(column =" eye_lmk_x_38")
   private Double  eyeLmkx38;
   @CsvBindByName(column =" eye_lmk_x_39")
   private Double  eyeLmkx39;
   @CsvBindByName(column =" eye_lmk_x_40")
   private Double  eyeLmkx40;
   @CsvBindByName(column ="eye_lmk_x_41")
   private Double  eyeLmkx41;
   @CsvBindByName(column =" eye_lmk_x_42")
   private Double  eyeLmkx42;
   @CsvBindByName(column =" eye_lmk_x_43")
   private Double  eyeLmkx43;
   @CsvBindByName(column ="eye_lmk_x_44")
   private Double  eyeLmkx44;
   @CsvBindByName(column ="eye_lmk_x_45")
   private Double  eyeLmkx45;
   @CsvBindByName(column ="eye_lmk_x_46")
   private Double  eyeLmkx46;
   @CsvBindByName(column =" eye_lmk_x_47")
   private Double  eyeLmkx47;
   @CsvBindByName(column =" eye_lmk_x_48")
   private Double  eyeLmkx48;
   @CsvBindByName(column =" eye_lmk_x_49")
   private Double  eyeLmkx49;
   @CsvBindByName(column ="eye_lmk_x_50")
   private Double  eyeLmkx50;
   @CsvBindByName(column ="eye_lmk_x_51")
   private Double  eyeLmkx51;
   @CsvBindByName(column =" eye_lmk_x_52")
   private Double  eyeLmkx52;
   @CsvBindByName(column ="eye_lmk_x_53")
   private Double  eyeLmkx53;
   @CsvBindByName(column =" eye_lmk_x_54")
   private Double  eyeLmkx54;
   @CsvBindByName(column =" eye_lmk_x_55")
   private Double  eyeLmkx55;
   @CsvBindByName(column ="eye_lmk_y_0")
   private Double  eyeLmky0;
   @CsvBindByName(column =" eye_lmk_y_1")
   private Double  eyeLmky1;
   @CsvBindByName(column =" eye_lmk_y_2")
   private Double  eyeLmky2;
   @CsvBindByName(column ="eye_lmk_y_3")
   private Double  eyeLmky3;
   @CsvBindByName(column =" eye_lmk_y_4")
   private Double  eyeLmky4;
   @CsvBindByName(column =" eye_lmk_y_5")
   private Double  eyeLmky5;
   @CsvBindByName(column ="eye_lmk_y_6")
   private Double  eyeLmky6;
   @CsvBindByName(column ="eye_lmk_y_7")
   private Double  eyeLmky7;
   @CsvBindByName(column =" eye_lmk_y_8")
   private Double  eyeLmky8;
   @CsvBindByName(column =" eye_lmk_y_9")
   private Double  eyeLmky9;
   @CsvBindByName(column =" eye_lmk_y_10")
   private Double  eyeLmky10;
   @CsvBindByName(column =" eye_lmk_y_11")
   private Double  eyeLmky11;
   @CsvBindByName(column =" eye_lmk_y_12")
   private Double  eyeLmky12;
   @CsvBindByName(column =" eye_lmk_y_13")
   private Double  eyeLmky13;
   @CsvBindByName(column =" eye_lmk_y_14")
   private Double  eyeLmky14;
   @CsvBindByName(column =" eye_lmk_y_15")
   private Double  eyeLmky15;
   @CsvBindByName(column ="eye_lmk_y_16")
   private Double  eyeLmky16;
   @CsvBindByName(column =" eye_lmk_y_17")
   private Double  eyeLmky17;
   @CsvBindByName(column ="eye_lmk_y_18")
   private Double  eyeLmky18;
   @CsvBindByName(column =" eye_lmk_y_19")
   private Double  eyeLmky19;
   @CsvBindByName(column =" eye_lmk_y_20")
   private Double  eyeLmky20;
   @CsvBindByName(column =" eye_lmk_y_21")
   private Double  eyeLmky21;
   @CsvBindByName(column =" eye_lmk_y_22")
   private Double  eyeLmky22;
   @CsvBindByName(column =" eye_lmk_y_23")
   private Double  eyeLmky23;
   @CsvBindByName(column =" eye_lmk_y_24")
   private Double  eyeLmky24;
   @CsvBindByName(column =" eye_lmk_y_25")
   private Double  eyeLmky25;
   @CsvBindByName(column =" eye_lmk_y_26")
   private Double  eyeLmky26;
   @CsvBindByName(column ="eye_lmk_y_27")
   private Double  eyeLmky27;
   @CsvBindByName(column ="eye_lmk_y_28")
   private Double  eyeLmky28;
   @CsvBindByName(column =" eye_lmk_y_29")
   private Double  eyeLmky29;
   @CsvBindByName(column =" eye_lmk_y_30")
   private Double  eyeLmky30;
   @CsvBindByName(column =" eye_lmk_y_31")
   private Double  eyeLmky31;
   @CsvBindByName(column =" eye_lmk_y_32")
   private Double  eyeLmky32;
   @CsvBindByName(column =" eye_lmk_y_33")
   private Double  eyeLmky33;
   @CsvBindByName(column ="eye_lmk_y_34")
   private Double  eyeLmky34;
   @CsvBindByName(column =" eye_lmk_y_35")
   private Double  eyeLmky35;
   @CsvBindByName(column =" eye_lmk_y_36")
   private Double  eyeLmky36;
   @CsvBindByName(column =" eye_lmk_y_37")
   private Double  eyeLmky37;
   @CsvBindByName(column =" eye_lmk_y_38")
   private Double  eyeLmky38;
   @CsvBindByName(column =" eye_lmk_y_39")
   private Double  eyeLmky39;
   @CsvBindByName(column ="eye_lmk_y_40")
   private Double  eyeLmky40;
   @CsvBindByName(column =" eye_lmk_y_41")
   private Double  eyeLmky41;
   @CsvBindByName(column =" eye_lmk_y_42")
   private Double  eyeLmky42;
   @CsvBindByName(column ="eye_lmk_y_43")
   private Double  eyeLmky43;
   @CsvBindByName(column ="eye_lmk_y_44")
   private Double  eyeLmky44;
   @CsvBindByName(column =" eye_lmk_y_45")
   private Double  eyeLmky45;
   @CsvBindByName(column =" eye_lmk_y_46")
   private Double  eyeLmky46;
   @CsvBindByName(column =" eye_lmk_y_47")
   private Double  eyeLmky47;
   @CsvBindByName(column =" eye_lmk_y_48")
   private Double  eyeLmky48;
   @CsvBindByName(column =" eye_lmk_y_49")
   private Double  eyeLmky49;
   @CsvBindByName(column ="eye_lmk_y_50")
   private Double  eyeLmky50;
   @CsvBindByName(column =" eye_lmk_y_51")
   private Double  eyeLmky51;
   @CsvBindByName(column =" eye_lmk_y_52")
   private Double  eyeLmky52;
   @CsvBindByName(column ="eye_lmk_y_53")
   private Double  eyeLmky53;
   @CsvBindByName(column =" eye_lmk_y_54")
   private Double  eyeLmky54;
   @CsvBindByName(column =" eye_lmk_y_55")
   private Double  eyeLmky55;
   @CsvBindByName(column =" eye_lmk_X_0")
   private Double  eyeLmkXy0;
   @CsvBindByName(column =" eye_lmk_X_1")
   private Double  eyeLmkXy1;
   @CsvBindByName(column ="eye_lmk_X_2")
   private Double  eyeLmkXy2;
   @CsvBindByName(column ="eye_lmk_X_3")
   private Double  eyeLmkXy3;
   @CsvBindByName(column ="eye_lmk_X_4")
   private Double  eyeLmkXy4;
   @CsvBindByName(column ="eye_lmk_X_5")
   private Double  eyeLmkXy5;
   @CsvBindByName(column ="eye_lmk_X_6")
   private Double  eyeLmkXy6;
   @CsvBindByName(column ="eye_lmk_X_7")
   private Double  eyeLmkXy7;
   @CsvBindByName(column ="eye_lmk_X_8")
   private Double  eyeLmkXy8;
   @CsvBindByName(column ="eye_lmk_X_9")
   private Double  eyeLmkXy9;
   @CsvBindByName(column ="eye_lmk_X_10")
   private Double  eyeLmkXy10;
   @CsvBindByName(column ="eye_lmk_X_11")
   private Double  eyeLmkXy11;
   @CsvBindByName(column ="eye_lmk_X_12")
   private Double  eyeLmkXy12;
   @CsvBindByName(column ="eye_lmk_X_13")
   private Double  eyeLmkXy13;
   @CsvBindByName(column ="eye_lmk_X_14")
   private Double  eyeLmkXy14;
   @CsvBindByName(column ="eye_lmk_X_15")
   private Double  eyeLmkXy15;
   @CsvBindByName(column ="eye_lmk_X_16")
   private Double  eyeLmkXy16;
   @CsvBindByName(column ="eye_lmk_X_17")
   private Double  eyeLmkXy17;
   @CsvBindByName(column ="eye_lmk_X_18")
   private Double  eyeLmkXy18;
   @CsvBindByName(column ="eye_lmk_X_19")
   private Double  eyeLmkXy19;
   @CsvBindByName(column ="eye_lmk_X_20")
   private Double  eyeLmkXy20;
   @CsvBindByName(column ="eye_lmk_X_21")
   private Double  eyeLmkXy21;
   @CsvBindByName(column ="eye_lmk_X_22")
   private Double  eyeLmkXy22;
   @CsvBindByName(column ="eye_lmk_X_23")
   private Double  eyeLmkXy23;
   @CsvBindByName(column ="eye_lmk_X_24")
   private Double  eyeLmkXy24;
   @CsvBindByName(column ="eye_lmk_X_25")
   private Double  eyeLmkXy25;
   @CsvBindByName(column ="eye_lmk_X_26")
   private Double  eyeLmkXy26;
   @CsvBindByName(column ="eye_lmk_X_27")
   private Double  eyeLmkXy27;
   @CsvBindByName(column ="eye_lmk_X_28")
   private Double  eyeLmkXy28;
   @CsvBindByName(column ="eye_lmk_X_29")
   private Double  eyeLmkXy29;
   @CsvBindByName(column ="eye_lmk_X_30")
   private Double  eyeLmkXy30;
   @CsvBindByName(column ="eye_lmk_X_31")
   private Double  eyeLmkXy31;
   @CsvBindByName(column ="eye_lmk_X_32")
   private Double  eyeLmkXy32;
   @CsvBindByName(column ="eye_lmk_X_33")
   private Double  eyeLmkXy33;
   @CsvBindByName(column ="eye_lmk_X_34")
   private Double  eyeLmkXy34;
   @CsvBindByName(column ="eye_lmk_X_35")
   private Double  eyeLmkXy35;
   @CsvBindByName(column ="eye_lmk_X_36")
   private Double  eyeLmkXy36;
   @CsvBindByName(column ="eye_lmk_X_37")
   private Double  eyeLmkXy37;
   @CsvBindByName(column ="eye_lmk_X_38")
   private Double  eyeLmkXy38;
   @CsvBindByName(column ="eye_lmk_X_39")
   private Double  eyeLmkXy39;
   @CsvBindByName(column ="eye_lmk_X_40")
   private Double  eyeLmkXy40;
   @CsvBindByName(column ="eye_lmk_X_41")
   private Double  eyeLmkXy41;
   @CsvBindByName(column ="eye_lmk_X_42")
   private Double  eyeLmkXy42;
   @CsvBindByName(column ="eye_lmk_X_43")
   private Double  eyeLmkXy43;
   @CsvBindByName(column ="eye_lmk_X_44")
   private Double  eyeLmkXy44;
   @CsvBindByName(column ="eye_lmk_X_45")
   private Double  eyeLmkXy45;
   @CsvBindByName(column ="eye_lmk_X_46")
   private Double  eyeLmkXy46;
   @CsvBindByName(column ="eye_lmk_X_47")
   private Double  eyeLmkXy47;
   @CsvBindByName(column ="eye_lmk_X_48")
   private Double  eyeLmkXy48;
   @CsvBindByName(column ="eye_lmk_X_49")
   private Double  eyeLmkXy49;
   @CsvBindByName(column ="eye_lmk_X_50")
   private Double  eyeLmkXy50;
   @CsvBindByName(column ="eye_lmk_X_51")
   private Double  eyeLmkXy51;
   @CsvBindByName(column ="eye_lmk_X_52")
   private Double  eyeLmkXy52;
   @CsvBindByName(column ="eye_lmk_X_53")
   private Double  eyeLmkXy53;
   @CsvBindByName(column ="eye_lmk_X_54")
   private Double  eyeLmkXy54;
   @CsvBindByName(column ="eye_lmk_X_55")
   private Double  eyeLmkXy55;
   @CsvBindByName(column ="eye_lmk_Y_0")
   private Double  eyeLmkYz0;
   @CsvBindByName(column ="eye_lmk_Y_1")
   private Double  eyeLmkYz1;
   @CsvBindByName(column ="eye_lmk_Y_2")
   private Double  eyeLmkYz2;
   @CsvBindByName(column ="eye_lmk_Y_3")
   private Double  eyeLmkYz3;
   @CsvBindByName(column ="eye_lmk_Y_4")
   private Double  eyeLmkYz4;
   @CsvBindByName(column ="eye_lmk_Y_5")
   private Double  eyeLmkYz5;
   @CsvBindByName(column ="eye_lmk_Y_6")
   private Double  eyeLmkYz6;
   @CsvBindByName(column ="eye_lmk_Y_7")
   private Double  eyeLmkYz7;
   @CsvBindByName(column ="eye_lmk_Y_8")
   private Double  eyeLmkYz8;
   @CsvBindByName(column ="eye_lmk_Y_9")
   private Double  eyeLmkYz9;
   @CsvBindByName(column ="eye_lmk_Y_10")
   private Double  eyeLmkYz10;
   @CsvBindByName(column ="eye_lmk_Y_11")
   private Double  eyeLmkYz11;
   @CsvBindByName(column ="eye_lmk_Y_12")
   private Double  eyeLmkYz12;
   @CsvBindByName(column ="eye_lmk_Y_13")
   private Double  eyeLmkYz13;
   @CsvBindByName(column ="eye_lmk_Y_14")
   private Double  eyeLmkYz14;
   @CsvBindByName(column ="eye_lmk_Y_15")
   private Double  eyeLmkYz15;
   @CsvBindByName(column ="eye_lmk_Y_16")
   private Double  eyeLmkYz16;
   @CsvBindByName(column ="eye_lmk_Y_17")
   private Double  eyeLmkYz17;
   @CsvBindByName(column ="eye_lmk_Y_18")
   private Double  eyeLmkYz18;
   @CsvBindByName(column ="eye_lmk_Y_19")
   private Double  eyeLmkYz19;
   @CsvBindByName(column ="eye_lmk_Y_20")
   private Double  eyeLmkYz20;
   @CsvBindByName(column ="eye_lmk_Y_21")
   private Double  eyeLmkYz21;
   @CsvBindByName(column ="eye_lmk_Y_22")
   private Double  eyeLmkYz22;
   @CsvBindByName(column ="eye_lmk_Y_23")
   private Double  eyeLmkYz23;
   @CsvBindByName(column ="eye_lmk_Y_24")
   private Double  eyeLmkYz24;
   @CsvBindByName(column ="eye_lmk_Y_25")
   private Double  eyeLmkYz25;
   @CsvBindByName(column ="eye_lmk_Y_26")
   private Double  eyeLmkYz26;
   @CsvBindByName(column ="eye_lmk_Y_27")
   private Double  eyeLmkYz27;
   @CsvBindByName(column ="eye_lmk_Y_28")
   private Double  eyeLmkYz28;
   @CsvBindByName(column ="eye_lmk_Y_29")
   private Double  eyeLmkYz29;
   @CsvBindByName(column ="eye_lmk_Y_30")
   private Double  eyeLmkYz30;
   @CsvBindByName(column ="eye_lmk_Y_31")
   private Double  eyeLmkYz31;
   @CsvBindByName(column ="eye_lmk_Y_32")
   private Double  eyeLmkYz32;
   @CsvBindByName(column ="eye_lmk_Y_33")
   private Double  eyeLmkYz33;
   @CsvBindByName(column ="eye_lmk_Y_34")
   private Double  eyeLmkYz34;
   @CsvBindByName(column ="eye_lmk_Y_35")
   private Double  eyeLmkYz35;
   @CsvBindByName(column ="eye_lmk_Y_36")
   private Double  eyeLmkYz36;
   @CsvBindByName(column ="eye_lmk_Y_37")
   private Double  eyeLmkYz37;
   @CsvBindByName(column ="eye_lmk_Y_38")
   private Double  eyeLmkYz38;
   @CsvBindByName(column ="eye_lmk_Y_39")
   private Double  eyeLmkYz39;
   @CsvBindByName(column ="eye_lmk_Y_40")
   private Double  eyeLmkYz40;
   @CsvBindByName(column ="eye_lmk_Y_41")
   private Double  eyeLmkYz41;
   @CsvBindByName(column ="eye_lmk_Y_42")
   private Double  eyeLmkYz42;
   @CsvBindByName(column ="eye_lmk_Y_43")
   private Double  eyeLmkYz43;
   @CsvBindByName(column ="eye_lmk_Y_44")
   private Double  eyeLmkYz44;
   @CsvBindByName(column ="eye_lmk_Y_45")
   private Double  eyeLmkYz45;
   @CsvBindByName(column ="eye_lmk_Y_46")
   private Double  eyeLmkYz46;
   @CsvBindByName(column ="eye_lmk_Y_47")
   private Double  eyeLmkYz47;
   @CsvBindByName(column ="eye_lmk_Y_48")
   private Double  eyeLmkYz48;
   @CsvBindByName(column ="eye_lmk_Y_49")
   private Double  eyeLmkYz49;
   @CsvBindByName(column ="eye_lmk_Y_50")
   private Double  eyeLmkYz50;
   @CsvBindByName(column ="eye_lmk_Y_51")
   private Double  eyeLmkYz51;
   @CsvBindByName(column ="eye_lmk_Y_52")
   private Double  eyeLmkYz52;
   @CsvBindByName(column ="eye_lmk_Y_53")
   private Double  eyeLmkYz53;
   @CsvBindByName(column ="eye_lmk_Y_54")
   private Double  eyeLmkYz54;
   @CsvBindByName(column ="eye_lmk_Y_55")
   private Double  eyeLmkYz55;
   @CsvBindByName(column ="eye_lmk_Z_0")
   private Double  eyeLmkZx0;
   @CsvBindByName(column ="eye_lmk_Z_1")
   private Double  eyeLmkZx1;
   @CsvBindByName(column ="eye_lmk_Z_2")
   private Double  eyeLmkZx2;
   @CsvBindByName(column ="eye_lmk_Z_3")
   private Double  eyeLmkZx3;
   @CsvBindByName(column ="eye_lmk_Z_4")
   private Double  eyeLmkZx4;
   @CsvBindByName(column ="eye_lmk_Z_5")
   private Double  eyeLmkZx5;
   @CsvBindByName(column ="eye_lmk_Z_6")
   private Double  eyeLmkZx6;
   @CsvBindByName(column ="eye_lmk_Z_7")
   private Double  eyeLmkZx7;
   @CsvBindByName(column ="eye_lmk_Z_8")
   private Double  eyeLmkZx8;
   @CsvBindByName(column ="eye_lmk_Z_9")
   private Double  eyeLmkZx9;
   @CsvBindByName(column ="eye_lmk_Z_10")
   private Double  eyeLmkZx10;
   @CsvBindByName(column ="eye_lmk_Z_11")
   private Double  eyeLmkZx11;
   @CsvBindByName(column ="eye_lmk_Z_12")
   private Double  eyeLmkZx12;
   @CsvBindByName(column ="eye_lmk_Z_13")
   private Double  eyeLmkZx13;
   @CsvBindByName(column ="eye_lmk_Z_14")
   private Double  eyeLmkZx14;
   @CsvBindByName(column ="eye_lmk_Z_15")
   private Double  eyeLmkZx15;
   @CsvBindByName(column ="eye_lmk_Z_16")
   private Double  eyeLmkZx16;
   @CsvBindByName(column ="eye_lmk_Z_17")
   private Double  eyeLmkZx17;
   @CsvBindByName(column ="eye_lmk_Z_18")
   private Double  eyeLmkZx18;
   @CsvBindByName(column ="eye_lmk_Z_19")
   private Double  eyeLmkZx19;
   @CsvBindByName(column ="eye_lmk_Z_20")
   private Double  eyeLmkZx20;
   @CsvBindByName(column ="eye_lmk_Z_21")
   private Double  eyeLmkZx21;
   @CsvBindByName(column ="eye_lmk_Z_22")
   private Double  eyeLmkZx22;
   @CsvBindByName(column ="eye_lmk_Z_23")
   private Double  eyeLmkZx23;
   @CsvBindByName(column ="eye_lmk_Z_24")
   private Double  eyeLmkZx24;
   @CsvBindByName(column ="eye_lmk_Z_25")
   private Double  eyeLmkZx25;
   @CsvBindByName(column ="eye_lmk_Z_26")
   private Double  eyeLmkZx26;
   @CsvBindByName(column ="eye_lmk_Z_27")
   private Double  eyeLmkZx27;
   @CsvBindByName(column ="eye_lmk_Z_28")
   private Double  eyeLmkZx28;
   @CsvBindByName(column ="eye_lmk_Z_29")
   private Double  eyeLmkZx29;
   @CsvBindByName(column ="eye_lmk_Z_30")
   private Double  eyeLmkZx30;
   @CsvBindByName(column ="eye_lmk_Z_31")
   private Double  eyeLmkZx31;
   @CsvBindByName(column ="eye_lmk_Z_32")
   private Double  eyeLmkZx32;
   @CsvBindByName(column ="eye_lmk_Z_33")
   private Double  eyeLmkZx33;
   @CsvBindByName(column ="eye_lmk_Z_34")
   private Double  eyeLmkZx34;
   @CsvBindByName(column ="eye_lmk_Z_35")
   private Double  eyeLmkZx35;
   @CsvBindByName(column ="eye_lmk_Z_36")
   private Double  eyeLmkZx36;
   @CsvBindByName(column ="eye_lmk_Z_37")
   private Double  eyeLmkZx37;
   @CsvBindByName(column ="eye_lmk_Z_38")
   private Double  eyeLmkZx38;
   @CsvBindByName(column ="eye_lmk_Z_39")
   private Double  eyeLmkZx39;
   @CsvBindByName(column ="eye_lmk_Z_40")
   private Double  eyeLmkZx40;
   @CsvBindByName(column ="eye_lmk_Z_41")
   private Double  eyeLmkZx41;
   @CsvBindByName(column ="eye_lmk_Z_42")
   private Double  eyeLmkZx42;
   @CsvBindByName(column ="eye_lmk_Z_43")
   private Double  eyeLmkZx43;
   @CsvBindByName(column ="eye_lmk_Z_44")
   private Double  eyeLmkZx44;
   @CsvBindByName(column ="eye_lmk_Z_45")
   private Double  eyeLmkZx45;
   @CsvBindByName(column ="eye_lmk_Z_46")
   private Double  eyeLmkZx46;
   @CsvBindByName(column ="eye_lmk_Z_47")
   private Double  eyeLmkZx47;
   @CsvBindByName(column ="eye_lmk_Z_48")
   private Double  eyeLmkZx48;
   @CsvBindByName(column ="eye_lmk_Z_49")
   private Double  eyeLmkZx49;
   @CsvBindByName(column ="eye_lmk_Z_50")
   private Double  eyeLmkZx50;
   @CsvBindByName(column ="eye_lmk_Z_51")
   private Double  eyeLmkZx51;
   @CsvBindByName(column ="eye_lmk_Z_52")
   private Double  eyeLmkZx52;
   @CsvBindByName(column ="eye_lmk_Z_53")
   private Double  eyeLmkZx53;
   @CsvBindByName(column ="eye_lmk_Z_54")
   private Double  eyeLmkZx54;
   @CsvBindByName(column ="eye_lmk_Z_55")
   private Double  eyeLmkZx55;
   @CsvBindByName(column ="pose_Tx")
   private Double  poseTx;
   @CsvBindByName(column ="pose_Ty")
   private Double  poseTy;
   @CsvBindByName(column ="pose_Tz")
   private Double  poseTz;
   @CsvBindByName(column ="pose_Rx")
   private Double  poseRx;
   @CsvBindByName(column ="pose_Ry")
   private Double  poseRy;
   @CsvBindByName(column ="pose_Rz")
   private Double  poseRz;
   @CsvBindByName(column ="x_0")
   private Double  x0;
   @CsvBindByName(column ="x_1")
   private Double  x1;
   @CsvBindByName(column ="x_2")
   private Double  x2;
   @CsvBindByName(column ="x_3")
   private Double  x3;
   @CsvBindByName(column ="x_4")
   private Double  x4;
   @CsvBindByName(column ="x_5")
   private Double  x5;
   @CsvBindByName(column ="x_6")
   private Double  x6;
   @CsvBindByName(column ="x_7")
   private Double  x7;
   @CsvBindByName(column ="x_8")
   private Double  x8;
   @CsvBindByName(column ="x_9")
   private Double  x9;
   @CsvBindByName(column ="x_10")
   private Double  x10;
   @CsvBindByName(column ="x_11")
   private Double  x11;
   @CsvBindByName(column ="x_12")
   private Double  x12;
   @CsvBindByName(column ="x_13")
   private Double  x13;
   @CsvBindByName(column ="x_14")
   private Double  x14;
   @CsvBindByName(column ="x_15")
   private Double  x15;
   @CsvBindByName(column ="x_16")
   private Double  x16;
   @CsvBindByName(column ="x_17")
   private Double  x17;
   @CsvBindByName(column ="x_18")
   private Double  x18;
   @CsvBindByName(column ="x_19")
   private Double  x19;
   @CsvBindByName(column ="x_20")
   private Double  x20;
   @CsvBindByName(column ="x_21")
   private Double  x21;
   @CsvBindByName(column ="x_22")
   private Double  x22;
   @CsvBindByName(column ="x_23")
   private Double  x23;
   @CsvBindByName(column ="x_24")
   private Double  x24;
   @CsvBindByName(column ="x_25")
   private Double  x25;
   @CsvBindByName(column ="x_26")
   private Double  x26;
   @CsvBindByName(column ="x_27")
   private Double  x27;
   @CsvBindByName(column ="x_28")
   private Double  x28;
   @CsvBindByName(column ="x_29")
   private Double  x29;
   @CsvBindByName(column ="x_30")
   private Double  x30;
   @CsvBindByName(column ="x_31")
   private Double  x31;
   @CsvBindByName(column ="x_32")
   private Double  x32;
   @CsvBindByName(column ="x_33")
   private Double  x33;
   @CsvBindByName(column ="x_34")
   private Double  x34;
   @CsvBindByName(column ="x_35")
   private Double  x35;
   @CsvBindByName(column ="x_36")
   private Double  x36;
   @CsvBindByName(column ="x_37")
   private Double  x37;
   @CsvBindByName(column ="x_38")
   private Double  x38;
   @CsvBindByName(column ="x_39")
   private Double  x39;
   @CsvBindByName(column ="x_40")
   private Double  x40;
   @CsvBindByName(column ="x_41")
   private Double  x41;
   @CsvBindByName(column ="x_42")
   private Double  x42;
   @CsvBindByName(column ="x_43")
   private Double  x43;
   @CsvBindByName(column ="x_44")
   private Double  x44;
   @CsvBindByName(column ="x_45")
   private Double  x45;
   @CsvBindByName(column ="x_46")
   private Double  x46;
   @CsvBindByName(column ="x_47")
   private Double  x47;
   @CsvBindByName(column ="x_48")
   private Double  x48;
   @CsvBindByName(column ="x_49")
   private Double  x49;
   @CsvBindByName(column ="x_50")
   private Double  x50;
   @CsvBindByName(column ="x_51")
   private Double  x51;
   @CsvBindByName(column ="x_52")
   private Double  x52;
   @CsvBindByName(column ="x_53")
   private Double  x53;
   @CsvBindByName(column ="x_54")
   private Double  x54;
   @CsvBindByName(column ="x_55")
   private Double  x55;
   @CsvBindByName(column ="x_56")
   private Double  x56;
   @CsvBindByName(column ="x_57")
   private Double  x57;
   @CsvBindByName(column ="x_58")
   private Double  x58;
   @CsvBindByName(column ="x_59")
   private Double  x59;
   @CsvBindByName(column ="x_60")
   private Double  x60;
   @CsvBindByName(column ="x_61")
   private Double  x61;
   @CsvBindByName(column ="x_62")
   private Double  x62;
   @CsvBindByName(column ="x_63")
   private Double  x63;
   @CsvBindByName(column ="x_64")
   private Double  x64;
   @CsvBindByName(column ="x_65")
   private Double  x65;
   @CsvBindByName(column ="x_66")
   private Double  x66;
   @CsvBindByName(column ="x_67")
   private Double  x67;
   @CsvBindByName(column ="y_0")
   private Double  y0;
   @CsvBindByName(column ="y_1")
   private Double  y1;
   @CsvBindByName(column ="y_2")
   private Double  y2;
   @CsvBindByName(column ="y_3")
   private Double  y3;
   @CsvBindByName(column ="y_4")
   private Double  y4;
   @CsvBindByName(column ="y_5")
   private Double  y5;
   @CsvBindByName(column ="y_6")
   private Double  y6;
   @CsvBindByName(column ="y_7")
   private Double  y7;
   @CsvBindByName(column ="y_8")
   private Double  y8;
   @CsvBindByName(column ="y_9")
   private Double  y9;
   @CsvBindByName(column ="y_10")
   private Double  y10;
   @CsvBindByName(column ="y_11")
   private Double  y11;
   @CsvBindByName(column ="y_12")
   private Double  y12;
   @CsvBindByName(column ="y_13")
   private Double  y13;
   @CsvBindByName(column ="y_14")
   private Double  y14;
   @CsvBindByName(column ="y_15")
   private Double  y15;
   @CsvBindByName(column ="y_16")
   private Double  y16;
   @CsvBindByName(column ="y_17")
   private Double  y17;
   @CsvBindByName(column ="y_18")
   private Double  y18;
   @CsvBindByName(column ="y_19")
   private Double  y19;
   @CsvBindByName(column ="y_20")
   private Double  y20;
   @CsvBindByName(column ="y_21")
   private Double  y21;
   @CsvBindByName(column ="y_22")
   private Double  y22;
   @CsvBindByName(column ="y_23")
   private Double  y23;
   @CsvBindByName(column ="y_24")
   private Double  y24;
   @CsvBindByName(column ="y_25")
   private Double  y25;
   @CsvBindByName(column ="y_26")
   private Double  y26;
   @CsvBindByName(column ="y_27")
   private Double  y27;
   @CsvBindByName(column ="y_28")
   private Double  y28;
   @CsvBindByName(column ="y_29")
   private Double  y29;
   @CsvBindByName(column ="y_30")
   private Double  y30;
   @CsvBindByName(column ="y_31")
   private Double  y31;
   @CsvBindByName(column ="y_32")
   private Double  y32;
   @CsvBindByName(column ="y_33")
   private Double  y33;
   @CsvBindByName(column ="y_34")
   private Double  y34;
   @CsvBindByName(column ="y_35")
   private Double  y35;
   @CsvBindByName(column ="y_36")
   private Double  y36;
   @CsvBindByName(column ="y_37")
   private Double  y37;
   @CsvBindByName(column ="y_38")
   private Double  y38;
   @CsvBindByName(column ="y_39")
   private Double  y39;
   @CsvBindByName(column ="y_40")
   private Double  y40;
   @CsvBindByName(column ="y_41")
   private Double  y41;
   @CsvBindByName(column ="y_42")
   private Double  y42;
   @CsvBindByName(column ="y_43")
   private Double  y43;
   @CsvBindByName(column ="y_44")
   private Double  y44;
   @CsvBindByName(column ="y_45")
   private Double  y45;
   @CsvBindByName(column ="y_46")
   private Double  y46;
   @CsvBindByName(column ="y_47")
   private Double  y47;
   @CsvBindByName(column ="y_48")
   private Double  y48;
   @CsvBindByName(column ="y_49")
   private Double  y49;
   @CsvBindByName(column ="y_50")
   private Double  y50;
   @CsvBindByName(column ="y_51")
   private Double  y51;
   @CsvBindByName(column ="y_52")
   private Double  y52;
   @CsvBindByName(column ="y_53")
   private Double  y53;
   @CsvBindByName(column ="y_54")
   private Double  y54;
   @CsvBindByName(column ="y_55")
   private Double  y55;
   @CsvBindByName(column ="y_56")
   private Double  y56;
   @CsvBindByName(column ="y_57")
   private Double  y57;
   @CsvBindByName(column ="y_58")
   private Double  y58;
   @CsvBindByName(column ="y_59")
   private Double  y59;
   @CsvBindByName(column ="y_60")
   private Double  y60;
   @CsvBindByName(column ="y_61")
   private Double  y61;
   @CsvBindByName(column ="y_62")
   private Double  y62;
   @CsvBindByName(column ="y_63")
   private Double  y63;
   @CsvBindByName(column ="y_64")
   private Double  y64;
   @CsvBindByName(column ="y_65")
   private Double  y65;
   @CsvBindByName(column ="y_66")
   private Double  y66;
   @CsvBindByName(column ="y_67")
   private Double  y67;
   @CsvBindByName(column ="X_0")
   private Double  xy0;
   @CsvBindByName(column ="X_1")
   private Double  xy1;
   @CsvBindByName(column ="X_2")
   private Double  xy2;
   @CsvBindByName(column ="X_3")
   private Double  xy3;
   @CsvBindByName(column ="X_4")
   private Double  xy4;
   @CsvBindByName(column ="X_5")
   private Double  xy5;
   @CsvBindByName(column ="X_6")
   private Double  xy6;
   @CsvBindByName(column ="X_7")
   private Double  xy7;
   @CsvBindByName(column ="X_8")
   private Double  xy8;
   @CsvBindByName(column ="X_9")
   private Double  xy9;
   @CsvBindByName(column ="X_10")
   private Double  xy10;
   @CsvBindByName(column ="X_11")
   private Double  xy11;
   @CsvBindByName(column ="X_12")
   private Double  xy12;
   @CsvBindByName(column ="X_13")
   private Double  xy13;
   @CsvBindByName(column ="X_14")
   private Double  xy14;
   @CsvBindByName(column ="X_15")
   private Double  xy15;
   @CsvBindByName(column ="X_16")
   private Double  xy16;
   @CsvBindByName(column ="X_17")
   private Double  xy17;
   @CsvBindByName(column ="X_18")
   private Double  xy18;
   @CsvBindByName(column ="X_19")
   private Double  xy19;
   @CsvBindByName(column ="X_20")
   private Double  xy20;
   @CsvBindByName(column ="X_21")
   private Double  xy21;
   @CsvBindByName(column ="X_22")
   private Double  xy22;
   @CsvBindByName(column ="X_23")
   private Double  xy23;
   @CsvBindByName(column ="X_24")
   private Double  xy24;
   @CsvBindByName(column ="X_25")
   private Double  xy25;
   @CsvBindByName(column ="X_26")
   private Double  xy26;
   @CsvBindByName(column ="X_27")
   private Double  xy27;
   @CsvBindByName(column ="X_28")
   private Double  xy28;
   @CsvBindByName(column ="X_29")
   private Double  xy29;
   @CsvBindByName(column ="X_30")
   private Double  xy30;
   @CsvBindByName(column ="X_31")
   private Double  xy31;
   @CsvBindByName(column ="X_32")
   private Double  xy32;
   @CsvBindByName(column ="X_33")
   private Double  xy33;
   @CsvBindByName(column ="X_34")
   private Double  xy34;
   @CsvBindByName(column ="X_35")
   private Double  xy35;
   @CsvBindByName(column ="X_36")
   private Double  xy36;
   @CsvBindByName(column ="X_37")
   private Double  xy37;
   @CsvBindByName(column ="X_38")
   private Double  xy38;
   @CsvBindByName(column ="X_39")
   private Double  xy39;
   @CsvBindByName(column ="X_40")
   private Double  xy40;
   @CsvBindByName(column ="X_41")
   private Double  xy41;
   @CsvBindByName(column ="X_42")
   private Double  xy42;
   @CsvBindByName(column ="X_43")
   private Double  xy43;
   @CsvBindByName(column ="X_44")
   private Double  xy44;
   @CsvBindByName(column ="X_45")
   private Double  xy45;
   @CsvBindByName(column ="X_46")
   private Double  xy46;
   @CsvBindByName(column ="X_47")
   private Double  xy47;
   @CsvBindByName(column ="X_48")
   private Double  xy48;
   @CsvBindByName(column ="X_49")
   private Double  xy49;
   @CsvBindByName(column ="X_50")
   private Double  xy50;
   @CsvBindByName(column ="X_51")
   private Double  xy51;
   @CsvBindByName(column ="X_52")
   private Double  xy52;
   @CsvBindByName(column ="X_53")
   private Double  xy53;
   @CsvBindByName(column ="X_54")
   private Double  xy54;
   @CsvBindByName(column ="X_55")
   private Double  xy55;
   @CsvBindByName(column ="X_56")
   private Double  xy56;
   @CsvBindByName(column ="X_57")
   private Double  xy57;
   @CsvBindByName(column ="X_58")
   private Double  xy58;
   @CsvBindByName(column ="X_59")
   private Double  xy59;
   @CsvBindByName(column ="X_60")
   private Double  xy60;
   @CsvBindByName(column ="X_61")
   private Double  xy61;
   @CsvBindByName(column ="X_62")
   private Double  xy62;
   @CsvBindByName(column ="X_63")
   private Double  xy63;
   @CsvBindByName(column ="X_64")
   private Double  xy64;
   @CsvBindByName(column ="X_65")
   private Double  xy65;
   @CsvBindByName(column ="X_66")
   private Double  xy66;
   @CsvBindByName(column ="X_67")
   private Double  xy67;
   @CsvBindByName(column ="Y_0")
   private Double  yz0;
   @CsvBindByName(column ="Y_1")
   private Double  yz1;
   @CsvBindByName(column ="Y_2")
   private Double  yz2;
   @CsvBindByName(column ="Y_3")
   private Double  yz3;
   @CsvBindByName(column ="Y_4")
   private Double  yz4;
   @CsvBindByName(column ="Y_5")
   private Double  yz5;
   @CsvBindByName(column ="Y_6")
   private Double  yz6;
   @CsvBindByName(column ="Y_7")
   private Double  yz7;
   @CsvBindByName(column ="Y_8")
   private Double  yz8;
   @CsvBindByName(column ="Y_9")
   private Double  yz9;
   @CsvBindByName(column ="Y_10")
   private Double  yz10;
   @CsvBindByName(column ="Y_11")
   private Double  yz11;
   @CsvBindByName(column ="Y_12")
   private Double  yz12;
   @CsvBindByName(column ="Y_13")
   private Double  yz13;
   @CsvBindByName(column ="Y_14")
   private Double  yz14;
   @CsvBindByName(column ="Y_15")
   private Double  yz15;
   @CsvBindByName(column ="Y_16")
   private Double  yz16;
   @CsvBindByName(column ="Y_17")
   private Double  yz17;
   @CsvBindByName(column ="Y_18")
   private Double  yz18;
   @CsvBindByName(column ="Y_19")
   private Double  yz19;
   @CsvBindByName(column ="Y_20")
   private Double  yz20;
   @CsvBindByName(column ="Y_21")
   private Double  yz21;
   @CsvBindByName(column ="Y_22")
   private Double  yz22;
   @CsvBindByName(column ="Y_23")
   private Double  yz23;
   @CsvBindByName(column ="Y_24")
   private Double  yz24;
   @CsvBindByName(column ="Y_25")
   private Double  yz25;
   @CsvBindByName(column ="Y_26")
   private Double  yz26;
   @CsvBindByName(column ="Y_27")
   private Double  yz27;
   @CsvBindByName(column ="Y_28")
   private Double  yz28;
   @CsvBindByName(column ="Y_29")
   private Double  yz29;
   @CsvBindByName(column ="Y_30")
   private Double  yz30;
   @CsvBindByName(column ="Y_31")
   private Double  yz31;
   @CsvBindByName(column ="Y_32")
   private Double  yz32;
   @CsvBindByName(column ="Y_33")
   private Double  yz33;
   @CsvBindByName(column ="Y_34")
   private Double  yz34;
   @CsvBindByName(column ="Y_35")
   private Double  yz35;
   @CsvBindByName(column ="Y_36")
   private Double  yz36;
   @CsvBindByName(column ="Y_37")
   private Double  yz37;
   @CsvBindByName(column ="Y_38")
   private Double  yz38;
   @CsvBindByName(column ="Y_39")
   private Double  yz39;
   @CsvBindByName(column ="Y_40")
   private Double  yz40;
   @CsvBindByName(column ="Y_41")
   private Double  yz41;
   @CsvBindByName(column ="Y_42")
   private Double  yz42;
   @CsvBindByName(column ="Y_43")
   private Double  yz43;
   @CsvBindByName(column ="Y_44")
   private Double  yz44;
   @CsvBindByName(column ="Y_45")
   private Double  yz45;
   @CsvBindByName(column ="Y_46")
   private Double  yz46;
   @CsvBindByName(column ="Y_47")
   private Double  yz47;
   @CsvBindByName(column ="Y_48")
   private Double  yz48;
   @CsvBindByName(column ="Y_49")
   private Double  yz49;
   @CsvBindByName(column ="Y_50")
   private Double  yz50;
   @CsvBindByName(column ="Y_51")
   private Double  yz51;
   @CsvBindByName(column ="Y_52")
   private Double  yz52;
   @CsvBindByName(column ="Y_53")
   private Double  yz53;
   @CsvBindByName(column ="Y_54")
   private Double  yz54;
   @CsvBindByName(column ="Y_55")
   private Double  yz55;
   @CsvBindByName(column ="Y_56")
   private Double  yz56;
   @CsvBindByName(column ="Y_57")
   private Double  yz57;
   @CsvBindByName(column ="Y_58")
   private Double  yz58;
   @CsvBindByName(column ="Y_59")
   private Double  yz59;
   @CsvBindByName(column ="Y_60")
   private Double  yz60;
   @CsvBindByName(column ="Y_61")
   private Double  yz61;
   @CsvBindByName(column ="Y_62")
   private Double  yz62;
   @CsvBindByName(column ="Y_63")
   private Double  yz63;
   @CsvBindByName(column ="Y_64")
   private Double  yz64;
   @CsvBindByName(column ="Y_65")
   private Double  yz65;
   @CsvBindByName(column ="Y_66")
   private Double  yz66;
   @CsvBindByName(column ="Y_67")
   private Double  yz67;
   @CsvBindByName(column ="Z_0")
   private Double  zx0;
   @CsvBindByName(column ="Z_1")
   private Double  zx1;
   @CsvBindByName(column ="Z_2")
   private Double  zx2;
   @CsvBindByName(column ="Z_3")
   private Double  zx3;
   @CsvBindByName(column ="Z_4")
   private Double  zx4;
   @CsvBindByName(column ="Z_5")
   private Double  zx5;
   @CsvBindByName(column ="Z_6")
   private Double  zx6;
   @CsvBindByName(column ="Z_7")
   private Double  zx7;
   @CsvBindByName(column ="Z_8")
   private Double  zx8;
   @CsvBindByName(column ="Z_9")
   private Double  zx9;
   @CsvBindByName(column ="Z_10")
   private Double  zx10;
   @CsvBindByName(column ="Z_11")
   private Double  zx11;
   @CsvBindByName(column ="Z_12")
   private Double  zx12;
   @CsvBindByName(column ="Z_13")
   private Double  zx13;
   @CsvBindByName(column ="Z_14")
   private Double  zx14;
   @CsvBindByName(column ="Z_15")
   private Double  zx15;
   @CsvBindByName(column ="Z_16")
   private Double  zx16;
   @CsvBindByName(column ="Z_17")
   private Double  zx17;
   @CsvBindByName(column ="Z_18")
   private Double  zx18;
   @CsvBindByName(column ="Z_19")
   private Double  zx19;
   @CsvBindByName(column ="Z_20")
   private Double  zx20;
   @CsvBindByName(column ="Z_21")
   private Double  zx21;
   @CsvBindByName(column ="Z_22")
   private Double  zx22;
   @CsvBindByName(column ="Z_23")
   private Double  zx23;
   @CsvBindByName(column ="Z_24")
   private Double  zx24;
   @CsvBindByName(column ="Z_25")
   private Double  zx25;
   @CsvBindByName(column ="Z_26")
   private Double  zx26;
   @CsvBindByName(column ="Z_27")
   private Double  zx27;
   @CsvBindByName(column ="Z_28")
   private Double  zx28;
   @CsvBindByName(column ="Z_29")
   private Double  zx29;
   @CsvBindByName(column ="Z_30")
   private Double  zx30;
   @CsvBindByName(column ="Z_31")
   private Double  zx31;
   @CsvBindByName(column ="Z_32")
   private Double  zx32;
   @CsvBindByName(column ="Z_33")
   private Double  zx33;
   @CsvBindByName(column ="Z_34")
   private Double  zx34;
   @CsvBindByName(column ="Z_35")
   private Double  zx35;
   @CsvBindByName(column ="Z_36")
   private Double  zx36;
   @CsvBindByName(column ="Z_37")
   private Double  zx37;
   @CsvBindByName(column ="Z_38")
   private Double  zx38;
   @CsvBindByName(column ="Z_39")
   private Double  zx39;
   @CsvBindByName(column ="Z_40")
   private Double  zx40;
   @CsvBindByName(column ="Z_41")
   private Double  zx41;
   @CsvBindByName(column ="Z_42")
   private Double  zx42;
   @CsvBindByName(column ="Z_43")
   private Double  zx43;
   @CsvBindByName(column ="Z_44")
   private Double  zx44;
   @CsvBindByName(column ="Z_45")
   private Double  zx45;
   @CsvBindByName(column ="Z_46")
   private Double  zx46;
   @CsvBindByName(column ="Z_47")
   private Double  zx47;
   @CsvBindByName(column ="Z_48")
   private Double  zx48;
   @CsvBindByName(column ="Z_49")
   private Double  zx49;
   @CsvBindByName(column ="Z_50")
   private Double  zx50;
   @CsvBindByName(column ="Z_51")
   private Double  zx51;
   @CsvBindByName(column ="Z_52")
   private Double  zx52;
   @CsvBindByName(column ="Z_53")
   private Double  zx53;
   @CsvBindByName(column ="Z_54")
   private Double  zx54;
   @CsvBindByName(column ="Z_55")
   private Double  zx55;
   @CsvBindByName(column ="Z_56")
   private Double  zx56;
   @CsvBindByName(column ="Z_57")
   private Double  zx57;
   @CsvBindByName(column ="Z_58")
   private Double  zx58;
   @CsvBindByName(column ="Z_59")
   private Double  zx59;
   @CsvBindByName(column ="Z_60")
   private Double  zx60;
   @CsvBindByName(column ="Z_61")
   private Double  zx61;
   @CsvBindByName(column ="Z_62")
   private Double  zx62;
   @CsvBindByName(column ="Z_63")
   private Double  zx63;
   @CsvBindByName(column ="Z_64")
   private Double  zx64;
   @CsvBindByName(column ="Z_65")
   private Double  zx65;
   @CsvBindByName(column ="Z_66")
   private Double  zx66;
   @CsvBindByName(column ="Z_67")
   private Double  zx67;
   @CsvBindByName(column ="p_scale")
   private Double  pScale;
   @CsvBindByName(column ="p_rx")
   private Double  pRx;
   @CsvBindByName(column ="p_ry")
   private Double  pRy;
   @CsvBindByName(column ="p_rz")
   private Double  pRz;
   @CsvBindByName(column ="p_tx")
   private Double  pTx;
   @CsvBindByName(column ="p_ty")
   private Double  pTy;
   @CsvBindByName(column ="p_0")
   private Double  p0;
   @CsvBindByName(column ="p_1")
   private Double  p1;
   @CsvBindByName(column ="p_2")
   private Double  p2;
   @CsvBindByName(column ="p_3")
   private Double  p3;
   @CsvBindByName(column ="p_4")
   private Double  p4;
   @CsvBindByName(column ="p_5")
   private Double  p5;
   @CsvBindByName(column ="p_6")
   private Double  p6;
   @CsvBindByName(column ="p_7")
   private Double  p7;
   @CsvBindByName(column ="p_8")
   private Double  p8;
   @CsvBindByName(column ="p_9")
   private Double  p9;
   @CsvBindByName(column ="p_10")
   private Double  p10;
   @CsvBindByName(column ="p_11")
   private Double  p11;
   @CsvBindByName(column ="p_12")
   private Double  p12;
   @CsvBindByName(column ="p_13")
   private Double  p13;
   @CsvBindByName(column ="p_14")
   private Double  p14;
   @CsvBindByName(column ="p_15")
   private Double  p15;
   @CsvBindByName(column ="p_16")
   private Double  p16;
   @CsvBindByName(column ="p_17")
   private Double  p17;
   @CsvBindByName(column ="p_18")
   private Double  p18;
   @CsvBindByName(column ="p_19")
   private Double  p19;
   @CsvBindByName(column ="p_20")
   private Double  p20;
   @CsvBindByName(column ="p_21")
   private Double  p21;
   @CsvBindByName(column ="p_22")
   private Double  p22;
   @CsvBindByName(column ="p_23")
   private Double  p23;
   @CsvBindByName(column ="p_24")
   private Double  p24;
   @CsvBindByName(column ="p_25")
   private Double  p25;
   @CsvBindByName(column ="p_26")
   private Double  p26;
   @CsvBindByName(column ="p_27")
   private Double  p27;
   @CsvBindByName(column ="p_28")
   private Double  p28;
   @CsvBindByName(column ="p_29")
   private Double  p29;
   @CsvBindByName(column ="p_30")
   private Double  p30;
   @CsvBindByName(column ="p_31")
   private Double  p31;
   @CsvBindByName(column ="p_32")
   private Double  p32;
   @CsvBindByName(column ="p_33")
   private Double  p33;
   @CsvBindByName(column ="AU01_r")
   private Double  au01r;
   @CsvBindByName(column ="AU02_r")
   private Double  au02r;
   @CsvBindByName(column ="AU04_r")
   private Double  au04r;
   @CsvBindByName(column ="AU05_r")
   private Double  au05r;
   @CsvBindByName(column ="AU06_r")
   private Double  au06r;
   @CsvBindByName(column ="AU07_r")
   private Double  au07r;
   @CsvBindByName(column ="AU09_r")
   private Double  au09r;
   @CsvBindByName(column ="AU10_r")
   private Double  au10r;
   @CsvBindByName(column ="AU12_r")
   private Double  au12r;
   @CsvBindByName(column ="AU14_r")
   private Double  au14r;
   @CsvBindByName(column ="AU15_r")
   private Double  au15r;
   @CsvBindByName(column ="AU17_r")
   private Double  au17r;
   @CsvBindByName(column ="AU20_r")
   private Double  au20r;
   @CsvBindByName(column ="AU23_r")
   private Double  au23r;
   @CsvBindByName(column ="AU25_r")
   private Double  au25r;
   @CsvBindByName(column ="AU26_r")
   private Double  au26r;
   @CsvBindByName(column ="AU45_r")
   private Double  au45r;
   @CsvBindByName(column ="AU01_c")
   private Double  au01c;
   @CsvBindByName(column ="AU02_c")
   private Double  au02c;
   @CsvBindByName(column ="AU04_c")
   private Double  au04c;
   @CsvBindByName(column ="AU05_c")
   private Double  au05c;
   @CsvBindByName(column ="AU06_c")
   private Double  au06c;
   @CsvBindByName(column ="AU07_c")
   private Double  au07c;
   @CsvBindByName(column ="AU09_c")
   private Double  au09c;
   @CsvBindByName(column ="AU10_c")
   private Double  au10c;
   @CsvBindByName(column ="AU12_c")
   private Double  au12c;
   @CsvBindByName(column ="AU14_c")
   private Double  au14c;
   @CsvBindByName(column ="AU15_c")
   private Double  au15c;
   @CsvBindByName(column ="AU17_c")
   private Double  au17c;
   @CsvBindByName(column ="AU20_c")
   private Double  au20c;
   @CsvBindByName(column ="AU23_c")
   private Double  au23c;
   @CsvBindByName(column ="AU25_c")
   private Double  au25c;
   @CsvBindByName(column ="AU26_c")
   private Double  au26c;
   @CsvBindByName(column ="AU28_c")
   private Double  au28c;
   @CsvBindByName(column ="AU45_c")
   private Double  au45c;


}
