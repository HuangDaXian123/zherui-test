package com.ruzhe.keji.test.entity;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "sftp")
@Data
public class SftpConfig {
    //ip

    private String host;
    //端口

    private int port;
    //连接sftp用户名
    private String username;
    //连接sftp密码
    private String password;
    //存放文件目录
    private String dir;
    //sftp 中文件名前缀
    private String fileNamePrefix;
    //从sftp 下载文件保存位置
    private String savePath;
}

