package com.ruzhe.keji.test.entity;

import com.opencsv.bean.CsvBindByName;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity //实体类的注解
@Getter
@Setter
@NoArgsConstructor
public class Video {
    @Id
    @GeneratedValue
    private Integer id;

    @CsvBindByName(column ="eye_lmk_x_0")
    private Double eyeLmkx0;
    @CsvBindByName(column ="eye_lmk_x_1")
    private Double eyeLmkx1;
    @CsvBindByName(column ="eye_lmk_x_2")
    private Double eyeLmkx2;
    @CsvBindByName(column ="eye_lmk_x_3")
    private Double eyeLmkx3;
    @CsvBindByName(column ="eye_lmk_x_4")
    private Double eyeLmkx4;
    @CsvBindByName(column ="eye_lmk_x_5")
    private Double eyeLmkx5;
    @CsvBindByName(column ="eye_lmk_x_6")
    private Double eyeLmkx6;
    @CsvBindByName(column ="eye_lmk_x_7")
    private Double eyeLmkx7;
    @CsvBindByName(column ="eye_lmk_x_8")
    private Double eyeLmkx8;
    @CsvBindByName(column ="eye_lmk_x_9")
    private Double eyeLmkx9;
    @CsvBindByName(column ="eye_lmk_x_10")
    private Double eyeLmkx10;
    @CsvBindByName(column ="eye_lmk_x_11")
    private Double eyeLmkx11;
    @CsvBindByName(column ="eye_lmk_x_12")
    private Double eyeLmkx12;
    @CsvBindByName(column ="eye_lmk_x_13")
    private Double eyeLmkx13;
    @CsvBindByName(column ="eye_lmk_x_14")
    private Double eyeLmkx14;
    @CsvBindByName(column ="eye_lmk_x_15")
    private Double eyeLmkx15;
    @CsvBindByName(column ="eye_lmk_x_16")
    private Double eyeLmkx16;
    @CsvBindByName(column ="eye_lmk_x_17")
    private Double eyeLmkx17;
    @CsvBindByName(column ="eye_lmk_x_18")
    private Double eyeLmkx18;
    @CsvBindByName(column ="eye_lmk_x_19")
    private Double eyeLmkx19;
    @CsvBindByName(column ="eye_lmk_x_20")
    private Double eyeLmkx20;
    @CsvBindByName(column ="eye_lmk_x_21")
    private Double eyeLmkx21;
    @CsvBindByName(column ="eye_lmk_x_22")
    private Double eyeLmkx22;
    @CsvBindByName(column ="eye_lmk_x_23")
    private Double eyeLmkx23;
    @CsvBindByName(column ="eye_lmk_x_24")
    private Double eyeLmkx24;
    @CsvBindByName(column ="eye_lmk_x_25")
    private Double eyeLmkx25;
    @CsvBindByName(column ="eye_lmk_x_26")
    private Double eyeLmkx26;
    @CsvBindByName(column ="eye_lmk_x_27")
    private Double eyeLmkx27;
    @CsvBindByName(column ="eye_lmk_x_28")
    private Double eyeLmkx28;
    @CsvBindByName(column ="eye_lmk_x_29")
    private Double eyeLmkx29;
    @CsvBindByName(column ="eye_lmk_x_30")
    private Double eyeLmkx30;
    @CsvBindByName(column ="eye_lmk_x_31")
    private Double eyeLmkx31;
    @CsvBindByName(column ="eye_lmk_x_32")
    private Double eyeLmkx32;
    @CsvBindByName(column ="eye_lmk_x_33")
    private Double eyeLmkx33;
    @CsvBindByName(column ="eye_lmk_x_34")
    private Double eyeLmkx34;
    @CsvBindByName(column ="eye_lmk_x_35")
    private Double eyeLmkx35;
    @CsvBindByName(column ="eye_lmk_x_36")
    private Double eyeLmkx36;
    @CsvBindByName(column ="eye_lmk_x_37")
    private Double eyeLmkx37;
    @CsvBindByName(column ="eye_lmk_x_38")
    private Double eyeLmkx38;
    @CsvBindByName(column ="eye_lmk_x_39")
    private Double eyeLmkx39;
    @CsvBindByName(column ="eye_lmk_x_40")
    private Double eyeLmkx40;
    @CsvBindByName(column ="eye_lmk_x_41")
    private Double eyeLmkx41;
    @CsvBindByName(column ="eye_lmk_x_42")
    private Double eyeLmkx42;
    @CsvBindByName(column ="eye_lmk_x_43")
    private Double eyeLmkx43;
    @CsvBindByName(column ="eye_lmk_x_44")
    private Double eyeLmkx44;
    @CsvBindByName(column ="eye_lmk_x_45")
    private Double eyeLmkx45;
    @CsvBindByName(column ="eye_lmk_x_46")
    private Double eyeLmkx46;
    @CsvBindByName(column ="eye_lmk_x_47")
    private Double eyeLmkx47;
    @CsvBindByName(column ="eye_lmk_x_48")
    private Double eyeLmkx48;
    @CsvBindByName(column ="eye_lmk_x_49")
    private Double eyeLmkx49;
    @CsvBindByName(column ="eye_lmk_x_50")
    private Double eyeLmkx50;
    @CsvBindByName(column ="eye_lmk_x_51")
    private Double eyeLmkx51;
    @CsvBindByName(column ="eye_lmk_x_52")
    private Double eyeLmkx52;
    @CsvBindByName(column ="eye_lmk_x_53")
    private Double eyeLmkx53;
    @CsvBindByName(column ="eye_lmk_x_54")
    private Double eyeLmkx54;
    @CsvBindByName(column ="eye_lmk_x_55")
    private Double eyeLmkx55;
    @CsvBindByName(column ="eye_lmk_y_0")
    private Double eyeLmky0;
    @CsvBindByName(column ="eye_lmk_y_1")
    private Double eyeLmky1;
    @CsvBindByName(column ="eye_lmk_y_2")
    private Double eyeLmky2;
    @CsvBindByName(column ="eye_lmk_y_3")
    private Double eyeLmky3;
    @CsvBindByName(column ="eye_lmk_y_4")
    private Double eyeLmky4;
    @CsvBindByName(column ="eye_lmk_y_5")
    private Double eyeLmky5;
    @CsvBindByName(column ="eye_lmk_y_6")
    private Double eyeLmky6;
    @CsvBindByName(column ="eye_lmk_y_7")
    private Double eyeLmky7;
    @CsvBindByName(column ="eye_lmk_y_8")
    private Double eyeLmky8;
    @CsvBindByName(column ="eye_lmk_y_9")
    private Double eyeLmky9;
    @CsvBindByName(column ="eye_lmk_y_10")
    private Double eyeLmky10;
    @CsvBindByName(column ="eye_lmk_y_11")
    private Double eyeLmky11;
    @CsvBindByName(column ="eye_lmk_y_12")
    private Double eyeLmky12;
    @CsvBindByName(column ="eye_lmk_y_13")
    private Double eyeLmky13;
    @CsvBindByName(column ="eye_lmk_y_14")
    private Double eyeLmky14;
    @CsvBindByName(column ="eye_lmk_y_15")
    private Double eyeLmky15;
    @CsvBindByName(column ="eye_lmk_y_16")
    private Double eyeLmky16;
    @CsvBindByName(column ="eye_lmk_y_17")
    private Double eyeLmky17;
    @CsvBindByName(column ="eye_lmk_y_18")
    private Double eyeLmky18;
    @CsvBindByName(column ="eye_lmk_y_19")
    private Double eyeLmky19;
    @CsvBindByName(column ="eye_lmk_y_20")
    private Double eyeLmky20;
    @CsvBindByName(column ="eye_lmk_y_21")
    private Double eyeLmky21;
    @CsvBindByName(column ="eye_lmk_y_22")
    private Double eyeLmky22;
    @CsvBindByName(column ="eye_lmk_y_23")
    private Double eyeLmky23;
    @CsvBindByName(column ="eye_lmk_y_24")
    private Double eyeLmky24;
    @CsvBindByName(column ="eye_lmk_y_25")
    private Double eyeLmky25;
    @CsvBindByName(column ="eye_lmk_y_26")
    private Double eyeLmky26;
    @CsvBindByName(column ="eye_lmk_y_27")
    private Double eyeLmky27;
    @CsvBindByName(column ="eye_lmk_y_28")
    private Double eyeLmky28;
    @CsvBindByName(column ="eye_lmk_y_29")
    private Double eyeLmky29;
    @CsvBindByName(column ="eye_lmk_y_30")
    private Double eyeLmky30;
    @CsvBindByName(column ="eye_lmk_y_31")
    private Double eyeLmky31;
    @CsvBindByName(column ="eye_lmk_y_32")
    private Double eyeLmky32;
    @CsvBindByName(column ="eye_lmk_y_33")
    private Double eyeLmky33;
    @CsvBindByName(column ="eye_lmk_y_34")
    private Double eyeLmky34;
    @CsvBindByName(column ="eye_lmk_y_35")
    private Double eyeLmky35;
    @CsvBindByName(column ="eye_lmk_y_36")
    private Double eyeLmky36;
    @CsvBindByName(column ="eye_lmk_y_37")
    private Double eyeLmky37;
    @CsvBindByName(column ="eye_lmk_y_38")
    private Double eyeLmky38;
    @CsvBindByName(column ="eye_lmk_y_39")
    private Double eyeLmky39;
    @CsvBindByName(column ="eye_lmk_y_40")
    private Double eyeLmky40;
    @CsvBindByName(column ="eye_lmk_y_41")
    private Double eyeLmky41;
    @CsvBindByName(column ="eye_lmk_y_42")
    private Double eyeLmky42;
    @CsvBindByName(column ="eye_lmk_y_43")
    private Double eyeLmky43;
    @CsvBindByName(column ="eye_lmk_y_44")
    private Double eyeLmky44;
    @CsvBindByName(column ="eye_lmk_y_45")
    private Double eyeLmky45;
    @CsvBindByName(column ="eye_lmk_y_46")
    private Double eyeLmky46;
    @CsvBindByName(column ="eye_lmk_y_47")
    private Double eyeLmky47;
    @CsvBindByName(column ="eye_lmk_y_48")
    private Double eyeLmky48;
    @CsvBindByName(column ="eye_lmk_y_49")
    private Double eyeLmky49;
    @CsvBindByName(column ="eye_lmk_y_50")
    private Double eyeLmky50;
    @CsvBindByName(column ="eye_lmk_y_51")
    private Double eyeLmky51;
    @CsvBindByName(column ="eye_lmk_y_52")
    private Double eyeLmky52;
    @CsvBindByName(column ="eye_lmk_y_53")
    private Double eyeLmky53;
    @CsvBindByName(column ="eye_lmk_y_54")
    private Double eyeLmky54;
    @CsvBindByName(column ="eye_lmk_y_55")
    private Double eyeLmky55;
}
