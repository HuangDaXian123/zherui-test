package com.ruzhe.keji.test.entity;

import com.opencsv.bean.CsvBindByPosition;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity //实体类的注解
@Getter
@Setter
@NoArgsConstructor
@Data
@Table(name="video_tf")
public class VideoTf {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private Integer userId;
    private Integer videoId;
    @Column(length = 100)
    private String createTime;

    @CsvBindByPosition(position =0,required = true)
    @Column(length=100)
    private String frame;
    @CsvBindByPosition(position =1,required = true)
    @Column(length=100)
    private String  faceId;
    @CsvBindByPosition(position =2,required = true)
    @Column(length=100)
    private String  timestamp;
    @CsvBindByPosition(position =3,required = true)
    @Column(length=100)
    private String  confidence;
    @CsvBindByPosition(position =4,required = true)
    @Column(length=100)
    private String  success;
    @CsvBindByPosition(position =5,required = true)
    @Column(length=100)
    private String  gaze0x;
    @CsvBindByPosition(position =6,required = true)
    @Column(length=100)
    private String  gaze0y;
    @CsvBindByPosition(position =7,required = true)
    @Column(length=100)
    private String  gaze0z;
    @CsvBindByPosition(position =8,required = true)
    @Column(length=100)
    private String  gaze1x;
    @CsvBindByPosition(position =9,required = true)
    @Column(length=100)
    private String  gaze1y;
    @CsvBindByPosition(position =10,required = true)
    @Column(length=100)
    private String  gaze1z;
    @CsvBindByPosition(position =11,required = true)
    @Column(length=100)
    private String  gazeAnglex;
    @CsvBindByPosition(position =12,required = true)
    @Column(length=100)
    private String  gazeAngley;
    @CsvBindByPosition(position =13,required = true)
    @Column(length=100)
    private String  eyeLmkx0;
    @CsvBindByPosition(position =14,required = true)
    @Column(length=100)
    private String  eyeLmkx1;
    @CsvBindByPosition(position =15,required = true)
    @Column(length=100)
    private String  eyeLmkx2;
    @CsvBindByPosition(position =16,required = true)
    @Column(length=100)
    private String  eyeLmkx3;
    @CsvBindByPosition(position =17,required = true)
    @Column(length=100)
    private String  eyeLmkx4;
    @CsvBindByPosition(position =18,required = true)
    @Column(length=100)
    private String  eyeLmkx5;
    @CsvBindByPosition(position =19,required = true)
    @Column(length=100)
    private String  eyeLmkx6;
    @CsvBindByPosition(position =20,required = true)
    @Column(length=100)
    private String  eyeLmkx7;
    @CsvBindByPosition(position =21,required = true)
    @Column(length=100)
    private String  eyeLmkx8;
    @CsvBindByPosition(position =22,required = true)
    @Column(length=100)
    private String  eyeLmkx9;
    @CsvBindByPosition(position =23,required = true)
    @Column(length=100)
    private String  eyeLmkx10;
    @CsvBindByPosition(position =24,required = true)
    @Column(length=100)
    private String  eyeLmkx11;
    @CsvBindByPosition(position =25,required = true)
    @Column(length=100)
    private String  eyeLmkx12;
    @CsvBindByPosition(position =26,required = true)
    @Column(length=100)
    private String  eyeLmkx13;
    @CsvBindByPosition(position =27,required = true)
    @Column(length=100)
    private String  eyeLmkx14;
    @CsvBindByPosition(position =28,required = true)
    @Column(length=100)
    private String  eyeLmkx15;
    @CsvBindByPosition(position =29,required = true)
    @Column(length=100)
    private String  eyeLmkx16;
    @CsvBindByPosition(position =30,required = true)
    @Column(length=100)
    private String  eyeLmkx17;
    @CsvBindByPosition(position =31,required = true)
    @Column(length=100)
    private String  eyeLmkx18;
    @CsvBindByPosition(position =32,required = true)
    @Column(length=100)
    private String  eyeLmkx19;
    @CsvBindByPosition(position =33,required = true)
    @Column(length=100)
    private String  eyeLmkx20;
    @CsvBindByPosition(position =34,required = true)
    @Column(length=100)
    private String  eyeLmkx21;
    @CsvBindByPosition(position =35,required = true)
    @Column(length=100)
    private String  eyeLmkx22;
    @CsvBindByPosition(position =36,required = true)
    @Column(length=100)
    private String  eyeLmkx23;
    @CsvBindByPosition(position =37,required = true)
    @Column(length=100)
    private String  eyeLmkx24;
    @CsvBindByPosition(position =38,required = true)
    @Column(length=100)
    private String  eyeLmkx25;
    @CsvBindByPosition(position =39,required = true)
    @Column(length=100)
    private String  eyeLmkx26;
    @CsvBindByPosition(position =40,required = true)
    @Column(length=100)
    private String  eyeLmkx27;
    @CsvBindByPosition(position =41,required = true)
    @Column(length=100)
    private String  eyeLmkx28;
    @CsvBindByPosition(position =42,required = true)
    @Column(length=100)
    private String  eyeLmkx29;
    @CsvBindByPosition(position =43,required = true)
    @Column(length=100)
    private String  eyeLmkx30;
    @CsvBindByPosition(position =44,required = true)
    @Column(length=100)
    private String  eyeLmkx31;
    @CsvBindByPosition(position =45,required = true)
    @Column(length=100)
    private String  eyeLmkx32;
    @CsvBindByPosition(position =46,required = true)
    @Column(length=100)
    private String  eyeLmkx33;
    @CsvBindByPosition(position =47,required = true)
    @Column(length=100)
    private String  eyeLmkx34;
    @CsvBindByPosition(position =48,required = true)
    @Column(length=100)
    private String  eyeLmkx35;
    @CsvBindByPosition(position =49,required = true)
    @Column(length=100)
    private String  eyeLmkx36;
    @CsvBindByPosition(position =50,required = true)
    @Column(length=100)
    private String  eyeLmkx37;
    @CsvBindByPosition(position =51,required = true)
    @Column(length=100)
    private String  eyeLmkx38;
    @CsvBindByPosition(position =52,required = true)
    @Column(length=100)
    private String  eyeLmkx39;
    @CsvBindByPosition(position =53,required = true)
    @Column(length=100)
    private String  eyeLmkx40;
    @CsvBindByPosition(position =54,required = true)
    @Column(length=100)
    private String  eyeLmkx41;
    @CsvBindByPosition(position =55,required = true)
    @Column(length=100)
    private String  eyeLmkx42;
    @CsvBindByPosition(position =56,required = true)
    @Column(length=100)
    private String  eyeLmkx43;
    @CsvBindByPosition(position =57,required = true)
    @Column(length=100)
    private String  eyeLmkx44;
    @CsvBindByPosition(position =58,required = true)
    @Column(length=100)
    private String  eyeLmkx45;
    @CsvBindByPosition(position =59,required = true)
    @Column(length=100)
    private String  eyeLmkx46;
    @CsvBindByPosition(position =60,required = true)
    @Column(length=100)
    private String  eyeLmkx47;
    @CsvBindByPosition(position =61,required = true)
    @Column(length=100)
    private String  eyeLmkx48;
    @CsvBindByPosition(position =62,required = true)
    @Column(length=100)
    private String  eyeLmkx49;
    @CsvBindByPosition(position =63,required = true)
    @Column(length=100)
    private String  eyeLmkx50;
    @CsvBindByPosition(position =64,required = true)
    @Column(length=100)
    private String  eyeLmkx51;
    @CsvBindByPosition(position =65,required = true)
    @Column(length=100)
    private String  eyeLmkx52;
    @CsvBindByPosition(position =66,required = true)
    @Column(length=100)
    private String  eyeLmkx53;
    @CsvBindByPosition(position =67,required = true)
    @Column(length=100)
    private String  eyeLmkx54;
    @CsvBindByPosition(position =68,required = true)
    @Column(length=100)
    private String  eyeLmkx55;
    @CsvBindByPosition(position =69,required = true)
    @Column(length=100)
    private String  eyeLmky0;
    @CsvBindByPosition(position =70,required = true)
    @Column(length=100)
    private String  eyeLmky1;
    @CsvBindByPosition(position =71,required = true)
    @Column(length=100)
    private String  eyeLmky2;
    @CsvBindByPosition(position =72,required = true)
    @Column(length=100)
    private String  eyeLmky3;
    @CsvBindByPosition(position =73,required = true)
    @Column(length=100)
    private String  eyeLmky4;
    @CsvBindByPosition(position =74,required = true)
    @Column(length=100)
    private String  eyeLmky5;
    @CsvBindByPosition(position =75,required = true)
    @Column(length=100)
    private String  eyeLmky6;
    @CsvBindByPosition(position =76,required = true)
    @Column(length=100)
    private String  eyeLmky7;
    @CsvBindByPosition(position =77,required = true)
    @Column(length=100)
    private String  eyeLmky8;
    @CsvBindByPosition(position =78,required = true)
    @Column(length=100)
    private String  eyeLmky9;
    @CsvBindByPosition(position =79,required = true)
    @Column(length=100)
    private String  eyeLmky10;
    @CsvBindByPosition(position =80,required = true)
    @Column(length=100)
    private String  eyeLmky11;
    @CsvBindByPosition(position =81,required = true)
    @Column(length=100)
    private String  eyeLmky12;
    @CsvBindByPosition(position =82,required = true)
    @Column(length=100)
    private String  eyeLmky13;
    @CsvBindByPosition(position =83,required = true)
    @Column(length=100)
    private String  eyeLmky14;
    @CsvBindByPosition(position =84,required = true)
    @Column(length=100)
    private String  eyeLmky15;
    @CsvBindByPosition(position =85,required = true)
    @Column(length=100)
    private String  eyeLmky16;
    @CsvBindByPosition(position =86,required = true)
    @Column(length=100)
    private String  eyeLmky17;
    @CsvBindByPosition(position =87,required = true)
    @Column(length=100)
    private String  eyeLmky18;
    @CsvBindByPosition(position =88,required = true)
    @Column(length=100)
    private String  eyeLmky19;
    @CsvBindByPosition(position =89,required = true)
    @Column(length=100)
    private String  eyeLmky20;
    @CsvBindByPosition(position =90,required = true)
    @Column(length=100)
    private String  eyeLmky21;
    @CsvBindByPosition(position =91,required = true)
    @Column(length=100)
    private String  eyeLmky22;
    @CsvBindByPosition(position =92,required = true)
    @Column(length=100)
    private String  eyeLmky23;
    @CsvBindByPosition(position =93,required = true)
    @Column(length=100)
    private String  eyeLmky24;
    @CsvBindByPosition(position =94,required = true)
    @Column(length=100)
    private String  eyeLmky25;
    @CsvBindByPosition(position =95,required = true)
    @Column(length=100)
    private String  eyeLmky26;
    @CsvBindByPosition(position =96,required = true)
    @Column(length=100)
    private String  eyeLmky27;
    @CsvBindByPosition(position =97,required = true)
    @Column(length=100)
    private String  eyeLmky28;
    @CsvBindByPosition(position =98,required = true)
    @Column(length=100)
    private String  eyeLmky29;
    @CsvBindByPosition(position =99,required = true)
    @Column(length=100)
    private String  eyeLmky30;
    @CsvBindByPosition(position =100,required = true)
    @Column(length=100)
    private String  eyeLmky31;
    @CsvBindByPosition(position =101,required = true)
    @Column(length=100)
    private String  eyeLmky32;
    @CsvBindByPosition(position =102,required = true)
    @Column(length=100)
    private String  eyeLmky33;
    @CsvBindByPosition(position =103,required = true)
    @Column(length=100)
    private String  eyeLmky34;
    @CsvBindByPosition(position =104,required = true)
    @Column(length=100)
    private String  eyeLmky35;
    @CsvBindByPosition(position =105,required = true)
    @Column(length=100)
    private String  eyeLmky36;
    @CsvBindByPosition(position =106,required = true)
    @Column(length=100)
    private String  eyeLmky37;
    @CsvBindByPosition(position =107,required = true)
    @Column(length=100)
    private String  eyeLmky38;
    @CsvBindByPosition(position =108,required = true)
    @Column(length=100)
    private String  eyeLmky39;
    @CsvBindByPosition(position =109,required = true)
    @Column(length=100)
    private String  eyeLmky40;
    @CsvBindByPosition(position =110,required = true)
    @Column(length=100)
    private String  eyeLmky41;
    @CsvBindByPosition(position =111,required = true)
    @Column(length=100)
    private String  eyeLmky42;
    @CsvBindByPosition(position =112,required = true)
    @Column(length=100)
    private String  eyeLmky43;
    @CsvBindByPosition(position =113,required = true)
    @Column(length=100)
    private String  eyeLmky44;
    @CsvBindByPosition(position =114,required = true)
    @Column(length=100)
    private String  eyeLmky45;
    @CsvBindByPosition(position =115,required = true)
    @Column(length=100)
    private String  eyeLmky46;
    @CsvBindByPosition(position =116,required = true)
    @Column(length=100)
    private String  eyeLmky47;
    @CsvBindByPosition(position =117,required = true)
    @Column(length=100)
    private String  eyeLmky48;
    @CsvBindByPosition(position =118,required = true)
    @Column(length=100)
    private String  eyeLmky49;
    @CsvBindByPosition(position =119,required = true)
    @Column(length=100)
    private String  eyeLmky50;
    @CsvBindByPosition(position =120,required = true)
    @Column(length=100)
    private String  eyeLmky51;
    @CsvBindByPosition(position =121,required = true)
    @Column(length=100)
    private String  eyeLmky52;
    @CsvBindByPosition(position =122,required = true)
    @Column(length=100)
    private String  eyeLmky53;
    @CsvBindByPosition(position =123,required = true)
    @Column(length=100)
    private String  eyeLmky54;
    @CsvBindByPosition(position =124,required = true)
    @Column(length=100)
    private String  eyeLmky55;



}
